import { View } from "react-native";
import { Provider } from "react-redux";
import store from "./src/redux/store";

import MyApp from "./src/MyApp";
export default function App() {
  return (
    <View style={{ flex: 1 }}>
      <Provider store={store}>
        <MyApp />
      </Provider>
    </View>
  );
}
