import { View, Text, TouchableOpacity } from "react-native";
import React from "react";
import { AntDesign } from "@expo/vector-icons";
const Header = () => {
  return (
    <View
      style={{
        width: "100%",
        position: "absolute",
        top: 0,
        zIndex: 10,
      }}
    >
      <TouchableOpacity
        style={{
          padding: 5,
          width: "11%",
          alignItems: "center",
        }}
      >
        <AntDesign
          name="left"
          size={30}
          style={{
            color: "white",
          }}
        />
      </TouchableOpacity>
    </View>
  );
};

export default Header;
