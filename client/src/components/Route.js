import Class from "../screens/Class";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
const Stack = createNativeStackNavigator();

const UiStack = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="Class" component={Class} />
    </Stack.Navigator>
  );
};

export default UiStack;
