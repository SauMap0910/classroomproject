import "react-native-gesture-handler";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { View, Text } from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import Classes from "../../screens/Classes";
import Profile from "../../screens/Profile";
import UiStack from "../Route";
import Setting from "../../screens/Setting";
const Tab = createBottomTabNavigator();

const screenOptions = ({ route }) => ({
  headerShown: false,

  tabBarIcon: ({ focused, color, size }) => {
    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "center",
          backgroundColor: focused ? "#54aed8" : "",
          padding: 4,
          borderRadius: 25,
        }}
      >
        <MaterialIcons
          style={{}}
          name={
            route.name == "Classes"
              ? "home"
              : route.name == "Profile"
              ? "account-circle"
              : route.name == "Setting"
              ? "settings"
              : ""
          }
          size={34}
          color={focused ? "white" : "#c9c9c9"}
        />
        <Text
          style={{
            alignSelf: "center",
            fontSize: 14,
            fontWeight: "400",
            marginHorizontal: 4,
            color: "white",
            display: focused ? "flex" : "none",
          }}
        >
          {route.name == "Classes"
            ? "Trang chủ"
            : route.name == "Profile"
            ? "Hồ sơ"
            : "Cài đặt"}
        </Text>
      </View>
    );
  },
});

function UiTab(props) {
  return (
    <Tab.Navigator screenOptions={screenOptions}>
      <Tab.Screen
        name={"Classes"}
        component={Classes}
        options={{
          tabBarLabelStyle: {},
          tabBarLabelStyle: {
            display: "none",
          },
        }}
      />
      <Tab.Screen
        name={"Profile"}
        component={Profile}
        options={{
          tabBarLabelStyle: {
            display: "none",
          },
        }}
      />
      <Tab.Screen
        name={"Stack"}
        component={UiStack}
        options={{
          tabBarItemStyle: {
            display: "none",
          },
        }}
      />
      <Tab.Screen
        name={"Setting"}
        component={Setting}
        options={{
          tabBarLabelStyle: {
            display: "none",
          },
        }}
      />
    </Tab.Navigator>
  );
}
export default UiTab;
