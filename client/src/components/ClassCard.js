import { View, Text, Image, TouchableOpacity } from "react-native";
import React from "react";
import styleGlobal from "../assets/Style";
import { WIDTH } from "../Const";
const ClassCard = ({ navigate, item, index, length }) => {
  const arrImg = [require("../assets/images/anhc.png"), require("../assets/images/anhb.png"), require("../assets/images/anha.png"),require('../assets/images/anhe.png'),require('../assets/images/anhf.png')]
  const arrColor = ['#6e7ebc','#54b0e1','#d2a959','#f0433c','#e0646e','#13181c']
  const img = arrImg[Math.floor(Math.random()*arrImg.length)]
  const color = arrColor[Math.floor(Math.random()*arrColor.length)]
  return (
    <View
      style={{
        maxHeight: 300,
        minHeight: 270,
        width: WIDTH / 1.6,
        backgroundColor:
         color,
        borderRadius: 25,
        marginRight: index === length ? 0 : 20,
      }}
    >
      <TouchableOpacity
        style={{
          borderRadius: 20,

          height: "100%",
          flex: 1,
        }}
        onPress={() => {
          navigate("Stack", {
            screen: 'Class',
            params: { item: item },
          });
        }}
      >
        <View style={{ flex: 2 }}>
          <Image
            style={{ width: "100%", height: "90%" }}
            source={
              img
            }
          />
        </View>
        <View style={{ flex: 1.5, marginHorizontal: 20 }}>
          <Text
            style={{
              fontSize: 24,
              color: "white",
              fontWeight: "400",
            }}
          >
            {item.className}
          </Text>
          <Text
            style={[styleGlobal.text, { color: "white", fontWeight: "300" }]}
          >
            {item.content}
          </Text>
          <Text
            style={[styleGlobal.text, { color: "white", fontWeight: "300" }]}
          >
            {item.description}
          </Text>
          <Text
            style={[styleGlobal.text, { color: "white", fontWeight: "300" }]}
          >
            {item.room}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default ClassCard;
