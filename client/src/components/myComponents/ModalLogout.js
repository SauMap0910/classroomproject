import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import React from "react";
import styleGlobal from "../../assets/Style";

import Modal from "react-native-modal";

const ModalLogout = ({ visible, content, title, cf, back, onPress }) => {
  return (
    <Modal
      isVisible={visible}
      backdropColor="#B4B3DB"
      backdropOpacity={0.8}
      animationIn="zoomInDown"
      animationOut="zoomOutUp"
      animationInTiming={600}
      animationOutTiming={600}
      backdropTransitionInTiming={600}
      backdropTransitionOutTiming={600}
      useNativeDriver={true}
    >
      <View
        style={{
          minHeight: "25%",
          maxHeight: "30%",
          backgroundColor: "white",
          borderRadius: 20,
        }}
      >
        <View style={{ margin: 20 }}>
          <Text
            style={{
              fontSize: 20,
              color: "#767679",
              fontWeight: "400",
              marginBottom: 20,
            }}
          >
            {title}
          </Text>
          <Text
            style={[styleGlobal.text, { color: "#747478", marginBottom: 20 }]}
          >
            {content}
          </Text>
          <View style={{ flexDirection: "row", justifyContent: "center" }}>
            <TouchableOpacity
              style={[styles.btn, { backgroundColor: "#fe81a0" }]}
              onPress={back}
            >
              <Text style={{ color: "white", fontSize: 16, textAlign: "center" }}>
                Trở lại
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.btn, { backgroundColor: "#dddddd" }]}
              onPress={onPress}
            >
              <Text
                style={{ color: "#919194", fontSize: 16, textAlign: "center" }}
              >
                {cf}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  btn: {
    padding: 10,
    width: 100,
    marginHorizontal: 10,
    borderRadius: 10,
  },
});

export default ModalLogout;
