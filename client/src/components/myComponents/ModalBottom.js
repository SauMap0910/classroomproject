import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import React from "react";
import { useDispatch } from "react-redux";
import { actionShowModal } from "../../redux/actionCreators";
const ModalBottom = ({ onPress, showModalFancy, line1, line2 }) => {
  const dispatch = useDispatch();
  return (
    <View
      style={{
        backgroundColor: "white",
        height: 165,
        borderTopStartRadius: 10,
        borderTopEndRadius: 10,
        borderColor: "#DADEE3",
      }}
    >
      <TouchableOpacity
        style={[styles.button]}
        onPress={() => {
          onPress();
          dispatch(actionShowModal(true));
        }}
      >
        <Text
          style={{
            fontSize: 16,
            textAlign: "center",
            color: "#54aed8",
          }}
        >
          Mở lớp
        </Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={[styles.button]}
        onPress={() => {
          onPress();
          showModalFancy();
        }}
      >
        <Text
          style={{
            fontSize: 16,
            textAlign: "center",
            color: "#54aed8",
          }}
        >
          Tham gia lớp
        </Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={[
          styles.button,
          {
            padding: 10,
            position: "absolute",
            bottom: 0,
          },
        ]}
      >
        <Text
          onPress={onPress}
          style={{ fontSize: 16, textAlign: "center", color: "red" }}
        >
          Đóng
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    width: "100%",
    justifyContent: "center",
    borderBottomWidth: 1,
    borderColor: "#c9c9c9",
    padding: 10,
    height: 55,
  },
});

export default ModalBottom;
