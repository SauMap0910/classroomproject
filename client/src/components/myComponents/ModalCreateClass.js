import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  ImageBackground,
  StyleSheet,
} from "react-native";
import React from "react";
import styleGlobal from "../../assets/Style";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { useDispatch, useSelector } from "react-redux";
import Modal from "react-native-modal";
import {
  AntDesign,
  MaterialCommunityIcons,
  MaterialIcons,
} from "@expo/vector-icons";
import { actionSaveClass, actionShowModal, OnEdit } from "../../redux/actionCreators";
import { useState } from "react";
import { api_get, api_post, api_put, URL_API } from "../../helper/Api";
import { API } from "../../Config";
import { useEffect } from "react";

const ModalCreateClass = ({navigate}) => {
  const { token, onEdit } = useSelector((state) => state);
  const { showModal } = useSelector((state) => state);
  const [className, setClassName] = useState("");
  const [content, setContent] = useState("");
  const [description, setDesc] = useState("");
  const [room, setRoom] = useState("");
  const dispatch = useDispatch();
  useEffect(() => {
    if(onEdit.OnEdit) {
      setClassName(onEdit.className
        )
        setContent(onEdit.content
        )
        setDesc(onEdit.description)
        setRoom(onEdit.room)
    }
    
  }, [onEdit])
  const createClass = () => {
    let request = API + URL_API.CREATE_CLASS;
    const data = {
      className,
      content,
      room,
      description,
      account_token: token,
    };
    api_post(dispatch, request, data, (res) => {
      dispatch(actionShowModal(false));
      let request = API + URL_API.GET_ALL_CLASS;
      api_get(dispatch, request, token, (res) => {
        dispatch(actionSaveClass(res.data))
      })
    });
  };
  const updateClass = () => {
    let request = API + URL_API.UPDATE_CLASS ;
    const data = {
      className,
      content,
      room,
      description,
      account_token: token,
      id: onEdit.id
    };
    api_put(dispatch, request, data, (res) => {
      dispatch(actionShowModal(false));
      let request = API + URL_API.GET_ALL_CLASS;
      api_get(dispatch, request, token, (res) => {
        dispatch(actionSaveClass(res.data))
        navigate('UiTab', {screen:'Classes'})
      })
    });
  };

  const closeModal = () => {
    dispatch(OnEdit({ OnEdit: false }))
    dispatch(actionShowModal(false))
  }
  return (
    <Modal
      isVisible={showModal}
      backdropColor="#B4B3DB"
      backdropOpacity={0.8}
      animationIn="zoomInDown"
      animationOut="zoomOutUp"
      animationInTiming={600}
      animationOutTiming={600}
      backdropTransitionInTiming={600}
      backdropTransitionOutTiming={600}
      useNativeDriver={true}
    >
      <View
        style={{
          height: "100%",
          backgroundColor: "white",
          flex: 1,
          borderRadius: 25,
        }}
      >
        <ImageBackground
          source={require("../../assets/images/page.jpg")}
          imageStyle={{
            borderRadius: 25,
          }}
          style={{ height: "100%", width: "100%" }}
        >
          <View style={{ height: "11.5%", justifyContent: "center" }}>
            <TouchableOpacity
              onPress={closeModal}
              style={{
                padding: 10,
                position: "absolute",
                top: 4,
                zIndex: 10,
              }}
            >
              <AntDesign name="close" size={24} color="white" />
            </TouchableOpacity>
            <Text
              style={{
                fontSize: 32,
                fontWeight: "bold",
                textAlign: "center",
                color: "white",
              }}
            >
              {onEdit.OnEdit ? 'Sửa lớp học' : 'Tạo lớp học'}
            </Text>
          </View>
          <View
            style={{
              marginHorizontal: "14%",
              marginTop: 2,
              height: "75%",
              justifyContent: "center",
            }}
          >
            <KeyboardAwareScrollView>
              <Image
                style={{ height: 200, width: "100%", marginTop: 2 }}
                source={require("../../assets/images/5836.jpg")}
              />
              <View style={styles.searchSection}>
                <MaterialCommunityIcons
                  name="google-classroom"
                  size={24}
                  color="black"
                  style={styles.searchIcon}
                />
                <TextInput
                  style={styles.input}
                  placeholder="Tên lớp học"
                  value={className}
                  onChangeText={(text) => {
                    setClassName(text);
                  }}
                />
              </View>

              <View style={styles.searchSection}>
                <MaterialCommunityIcons
                  name="content-paste"
                  size={24}
                  color="black"
                  style={styles.searchIcon}
                />
                <TextInput
                  multiline
                  style={styles.input}
                  placeholder="Nội dung"
                  value={content}
                  onChangeText={(text) => {
                    setContent(text);
                  }}
                />
              </View>

              <View style={styles.searchSection}>
                <MaterialIcons
                  name="description"
                  size={24}
                  color="black"
                  style={styles.searchIcon}
                />
                <TextInput
                  multiline
                  style={styles.input}
                  placeholder="Giới thiệu"
                  value={description}
                  onChangeText={(text) => {
                    setDesc(text);
                  }}
                />
              </View>
              <View style={styles.searchSection}>
                <MaterialIcons
                  name="meeting-room"
                  size={24}
                  color="black"
                  style={styles.searchIcon}
                />
                <TextInput
                  style={styles.input}
                  placeholder="Room"
                  value={room}
                  onChangeText={(text) => {
                    setRoom(text);
                  }}
                />
              </View>
              <TouchableOpacity
                style={{
                  marginVertical: 20,
                  alignItems: "center",
                }}
                onPress={ onEdit.OnEdit ? updateClass : createClass}
              >
                <Text
                  style={[
                    styleGlobal.text,
                    {
                      fontSize: 24,
                    },
                  ]}
                >
                  OK
                </Text>
              </TouchableOpacity>
            </KeyboardAwareScrollView>
          </View>
        </ImageBackground>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  searchSection: {
    marginVertical: 5,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#00000000",
  },
  searchIcon: {
    padding: 20,
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    color: "#424242",
    fontSize: 16,
  },
});

export default ModalCreateClass;
