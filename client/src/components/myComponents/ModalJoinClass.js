import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  ImageBackground,
  StyleSheet,
} from "react-native";
import React from "react";
import styleGlobal from "../../assets/Style";
import { WIDTH } from "../../Const";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { useDispatch, useSelector } from "react-redux";
import { api_get, api_put, inviteStudent, joinClassByClassCode, URL_API } from "../../helper/Api";
import { useState } from "react";
import { API } from "../../Config";
import { actionChangePopupNoti, actionSaveClass, ActionSaveClassStudent } from "../../redux/actionCreators";
import Modal from 'react-native-modal'
const ModalJoinClass = ({ title, content, back, visible,placeholder,id }) => {
  const dispatch = useDispatch();
  const [data, setData] = useState((state) => state);
  const { token } = useSelector((state) => state);
  const joinclass = () => {
    let body = { account_token: token,classCode: data };
    joinClassByClassCode(dispatch, body, (res) => {
      if (res.status === 200) {
        let request = API + URL_API.GET_ALL_CLASS;
        api_get(dispatch, request, token, (res) => {
          back()
          dispatch(actionChangePopupNoti("Vào lớp thành công!!"))
          dispatch(actionSaveClass(res.data))
        })
      }
    })
  };

  const getAllstudent = () => {
    let request = API + URL_API.GET_STUDENTS_CLASS + `?classId=${id}`
    api_get(dispatch, request, token, (res) => {
      dispatch(ActionSaveClassStudent(res.data))
    })
  }


  const invite = () => {
    let body = { account_token: token, classId:id , email:data }
    inviteStudent(dispatch,body,res => {
      back()
      dispatch(actionChangePopupNoti('Mời thành công'))
      getAllstudent()
    })
  }
  return (
    <Modal
      isVisible={visible}
      backdropColor="#B4B3DB"
      backdropOpacity={0.8}
      animationIn="zoomInDown"
      animationOut="zoomOutUp"
      animationInTiming={600}
      animationOutTiming={600}
      backdropTransitionInTiming={600}
      backdropTransitionOutTiming={600}
      useNativeDriver={true}
    >
      <View
        style={{
          minHeight: "70%",
          maxHeight: "80%",
          backgroundColor: "white",
          borderRadius: 25,
          flex: 1,
        }}
      >
        <View style={{ flex: 3 }}>
          <ImageBackground
            style={{ height: "100%", width: "100%", borderRadius: 50 }}
            source={require("../../assets/images/bg.jpg")}
            imageStyle={{ borderTopLeftRadius: 25, borderTopRightRadius: 25 }}
          >
            <Image
              source={require("../../assets/images/anhd.png")}
              style={{
                maxHeight: 150,
                minHeight: 120,
                width: WIDTH / 2.5,
                position: "absolute",
                alignSelf: "center",
                top: -40,
                right: -30,
              }}
            />
          </ImageBackground>
        </View>
        <View style={{ flex: 4 }}>
          <KeyboardAwareScrollView>
            <View style={{ flex: 4, marginHorizontal: 20, marginTop: 20 }}>
              <Text
                style={{
                  fontSize: 24,
                  fontWeight: "500",
                  textAlign: "center",
                  color: "#575f63",
                }}
              >
                {title}
              </Text>
              <Text
                style={{
                  fontSize: 16,
                  textAlign: "center",
                  marginVertical: 20,
                  color: "#c9c9c9",
                }}
              >
                {content}
              </Text>
              <TextInput
                placeholder={placeholder}
                value={data}
                onChangeText={(text) => {
                  setData(text);
                }}
                style={{
                  padding: 15,
                  borderRadius: 25,
                  borderWidth: 1,
                }}
              />

              <Text
                style={[
                  styleGlobal.text,
                  { color: "#c9c9c9", marginVertical: 20 },
                ]}
              >
                Nếu bạn đang gặp vấn đề khi tham gia lớp học, hãy chuyển đến{" "}
                <Text style={{ color: "#6bb0cc" }}>
                  bài viết trong Trung tâm trợ giúp
                </Text>
              </Text>
            </View>
            <View
              style={{
                borderTopWidth: 1,
                borderColor: "#c9c9c9",
                flexDirection: "row",
                justifyContent: "space-around",
                flex: 1,
              }}
            >
              <TouchableOpacity style={styles.btn} onPress={title == "Tham gia lớp học" ? joinclass : invite}>
                <Text style={[styles.text, { color: "#6bb0cc" }]}>OK</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.btn} onPress={back}>
                <Text style={[styles.text, { color: "#c9c9c9" }]}>Hủy</Text>
              </TouchableOpacity>
            </View>
          </KeyboardAwareScrollView>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  btn: {
    padding: 15,
  },
  text: {
    fontSize: 18,
    fontWeight: "bold",
  },
});

export default ModalJoinClass;
