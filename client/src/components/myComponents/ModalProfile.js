import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  ImageBackground,
  StyleSheet,
} from "react-native";
import React from "react";
import styleGlobal from "../../assets/Style";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { useDispatch, useSelector } from "react-redux";
import Modal from "react-native-modal";
import {
  actionChangePopupNoti,
  ActionshowEditUser,
  getAlert,
} from "../../redux/actionCreators";
import { AntDesign, Entypo } from "@expo/vector-icons";
import * as ImagePicker from "expo-image-picker";
import { useState } from "react";
import { api_updateProfile } from "../../helper/Api";

const ModalProfile = () => {
  const { user, token,alert } = useSelector((state) => state);
  const [avatar, setAvatar] = useState(user.avatar);
  const [username, setUsername] = useState(user.displayname);
  const [email, setEmail] = useState(user.email);
  const [err, setErr] = useState('')
  const { editUserP } = useSelector((state) => state);

  function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


  const dispatch = useDispatch();
  const pickImage = async () => {
    const perMission = await ImagePicker.requestMediaLibraryPermissionsAsync();
    if (perMission.granted === false) {
      dispatch(
        actionChangePopupNoti("Bạn vui lòng cấp quyền truy cập vào thư viện!")
      );
      return;
    }

    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [3, 3],
      quality: 0.5,
      //   base64: true,
    });
    if (!result.cancelled) {
      setAvatar(result.uri);
    }
  };

  const updateProfile = () => {
    const data = {
      email,
      displayname: username,
      avatar,
      account_token: token
    };
    if (alert ) {
      setTimeout(() => {
        dispatch(getAlert({}));
      }, 2000);
    }
    const valid = validateEmail(email)
    if(valid)  {
      setErr('')
    api_updateProfile(dispatch, user.id, data);
    } else {
      setErr('Email sai định dạng')
    }
  };



  return (
    <Modal
      isVisible={editUserP}
      backdropColor="#B4B3DB"
      backdropOpacity={0.8}
      animationIn="zoomInDown"
      animationOut="zoomOutUp"
      animationInTiming={600}
      animationOutTiming={600}
      backdropTransitionInTiming={600}
      backdropTransitionOutTiming={600}
      useNativeDriver={true}
    >
      <View
        style={{
          height: "100%",
          backgroundColor: "white",
          flex: 1,
          borderRadius: 25,
        }}
      >
        <ImageBackground
          source={require("../../assets/images/page.jpg")}
          imageStyle={{
            borderRadius: 25,
          }}
          style={{ height: "100%", width: "100%" }}
        >
          <View style={{ height: "11.5%", justifyContent: "center" }}>
            <TouchableOpacity
              onPress={() => {
                dispatch(ActionshowEditUser(false));
              }}
              style={{
                padding: 10,
                position: "absolute",
                top: 4,
                zIndex: 10,
              }}
            >
              <AntDesign name="close" size={24} color="white" />
            </TouchableOpacity>
            <Text
              style={{
                fontSize: 32,
                fontWeight: "bold",
                textAlign: "center",
                color: "white",
              }}
            >
              Cập nhật hồ sơ
            </Text>
          </View>
          <View
            style={{
              marginHorizontal: "14%",
              marginTop: 2,
              height: "75%",
              justifyContent: "center",
            }}
          >
            <KeyboardAwareScrollView>
              {avatar ? (
                <TouchableOpacity onPress={pickImage}>
                  <Image
                    source={{
                      uri: avatar,
                    }}
                    style={{
                      width: "100%",
                      height: undefined,
                      aspectRatio: 1 / 1,
                      borderRadius: 50,
                    }}
                  />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  style={{
                    width: "100%",
                    height: undefined,
                    aspectRatio: 1 / 1,
                    borderRadius: 50,
                    backgroundColor: "#f5f5f5",
                    borderWidth: 1,
                    borderStyle: "dashed",
                    borderRadius: 10,
                  }}
                  onPress={pickImage}
                ></TouchableOpacity>
              )}
              <View style={styles.searchSection}>
                <AntDesign
                  name="idcard"
                  size={24}
                  color="black"
                  style={styles.searchIcon}
                />

                <TextInput
                  style={styles.input}
                  placeholder="Tên người dùng"
                  value={username}
                  onChangeText={(text) => {
                    setUsername(text);
                  }}
                />
              </View>

              <View style={styles.searchSection}>
                <Entypo
                  name="email"
                  size={24}
                  style={styles.searchIcon}
                  color="black"
                />
                <TextInput
                  style={styles.input}
                  placeholder="Email"
                  value={email}
                  onChangeText={(text) => {
                    setEmail(text);
                  }}
                />
              </View>
              <Text style={{color:'red', textAlign:'center'}}>{err ? err : ""}</Text>

              <TouchableOpacity
                onPress={updateProfile}
                style={{
                  marginVertical: 20,
                  alignItems: "center",
                }}
              >
                <Text
                  style={[
                    styleGlobal.text,
                    {
                      fontSize: 24,
                    },
                  ]}
                >
                  OK
                </Text>
              </TouchableOpacity>
            </KeyboardAwareScrollView>
          </View>
        </ImageBackground>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  searchSection: {
    marginVertical: 5,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#00000000",
  },
  searchIcon: {
    padding: 20,
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    color: "#424242",
    fontSize: 16,
  },
});

export default ModalProfile;
