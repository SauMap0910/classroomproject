import { View, Text, Image, TextInput, TouchableOpacity } from "react-native";
import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import styleGlobal from "../../assets/Style";
import { Ionicons } from "@expo/vector-icons";
import SelectDropdown from "react-native-select-dropdown";
import { Feather, AntDesign } from "@expo/vector-icons";
import moment from 'moment';
import { api_delete, api_get, URL_API } from "../../helper/Api";
import { API } from "../../Config";
import { ActionSavePost } from "../../redux/actionCreators";
const Status = ({ post }) => {
  const dispatch = useDispatch()
  const { user, token, detailClass } = useSelector((state) => state);
   const deletePost = () => {
    let request = API + URL_API.DELETE_POST + `?id=${post.id}`
    api_delete(dispatch, request, token, res => {
      let request = API + URL_API.GET_POST + `?classId=${detailClass.id}`
      api_get(dispatch, request, token, (res) => {
        dispatch(ActionSavePost(res.data))
      })
    })
  }

  const dropdown = ["Sửa", "Xóa"]
  return (
    <View
      style={{
        padding: 5,
        borderWidth: 1,
        borderRadius: 15,
        borderColor: "#e4e6e9",
        marginVertical: 5,
      }} key={post.id}
    >
      <View style={{ flexDirection: "row", alignItems: "center" }}>
        <Image
          style={{
            height: 50,
            width: 50,
            borderRadius: 50,
            marginRight: 10,
          }}
          source={user.avatar ? { uri: post.avatar } : require('../../assets/images/avatar.png')}
        />
        <View>
          <Text style={[styleGlobal.text, { color: "black" }]}>
            {post.displayName}
          </Text>
          <Text style={{ fontSize: 12, color: '#dadce0' }}>{moment(post.createdDate).fromNow()}</Text>
        </View>

        <View style={{ flex: 1 }} />

        <SelectDropdown
          data={dropdown}
          onSelect={(selectedItem, index) => {
            selectedItem === "Xóa" ? deletePost() : ""
          }}

          dropdownStyle={{
            marginTop: -30,
            borderRadius: 20
          }}
          buttonStyle={{
            paddingHorizontal: 5,
            backgroundColor: 'transparent',
          }}

          renderCustomizedButtonChild={(selectedItem, index) => {
            return (
              <View
                style={{
                  alignItems: 'flex-end'
                }}
              >
                <AntDesign
                  name="ellipsis1"
                  size={24}
                  color="white"
                  style={{
                    textAlign: 'center',
                    padding: 5,
                    marginRight: 10,
                    backgroundColor: "rgba(52, 52, 52, 0.2)",
                    borderRadius: 50,
                    height: 35, width: 35
                  }}
                />
              </View>
            );
          }}
          renderCustomizedRowChild={(item, index) => {

            return (
              <View
              >
                <Text style={[styleGlobal.text, { textAlign: 'center' }]}>{item}</Text>
              </View>
            );
          }}
          buttonTextAfterSelection={(selectedItem, index) => {
            return selectedItem;
          }}
          rowTextForSelection={(item, index) => {
            return item;
          }}

        />
      </View>
      <Text style={{ marginVertical: 5 }}>{post.body}</Text>
      { (post?.title.slice(post?.title.indexOf('.') + 1) !== 'jpg' && post?.title != 1) ?

        <TouchableOpacity style={{ flexDirection: 'row', borderColor: '#e9eaea', borderWidth: 1 }}>
          <View style={{ padding: 10, borderRightWidth: 1, borderColor: '#e9eaea' }}>
            <Image style={{ width: 40, height: 40 }} source={require('../../assets/images/google-drive.png')} />
          </View>
          <View style={{ borderColor: '#e9eaea', padding: 10, }}>
            <Text>{post?.title}</Text>
            <Text style={{
              textTransform: 'uppercase'
            }}>{post?.title.slice(post?.title.indexOf('.') + 1)}</Text>

          </View>
        </TouchableOpacity>
        : post?.topic != 1 ?
        <Image
          style={{
            margin: 1,
            borderWidth: 1,
            borderRadius: 10,
            width: "100%",
            height: undefined,
            aspectRatio: 1 / 1,
          }}
          resizeMode="stretch"
          source={{ uri: post.topic }}
        /> : null

      }

      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          borderTopWidth: 1,
          borderColor: "#dadce0",
          padding: 10,
          justifyContent: "space-around",
        }}
      >
        <Image
          style={{
            height: 40,
            width: 40,
            borderRadius: 50,
            marginRight: 10,
          }}
          source={user.avatar ? { uri: user.avatar } : require("../../assets/images/avatar.png")}
        />


        <View
          style={{
            flexDirection: "row",
            borderRadius: 15,
            borderWidth: 1,
            alignSelf: "center",
            borderColor: "#dadce0",
          }}
        >
          <TextInput
            placeholder="Thêm nhận xét"
            style={{
              maxWidth: "100%",
              minWidth: "65%",
              padding: 6,
            }}
            multiline={true}
            scrollEnabled={false}
          />

          <TouchableOpacity style={{ padding: 10 }}>
            <Feather name="send" size={24} color="#dadce0" />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Status;
