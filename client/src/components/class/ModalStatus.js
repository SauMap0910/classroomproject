import { View, Text, TouchableOpacity, Image } from "react-native";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import styleGlobal from "../../assets/Style";
import { ActionshowStatus } from "../../redux/actionCreators";

const ModalStatus = () => {
  const { user } = useSelector((state) => state);
  const dispatch = useDispatch();
  return (
    <TouchableOpacity
      onPress={() => {
        dispatch(ActionshowStatus(true));
      }}
      style={{
        flexDirection: "row",
        alignItems: "center",
        padding: 5,
        borderWidth: 1,
        borderRadius: 15,
        borderColor: "#e4e6e9",
        marginVertical: 5,
      }}
    >
      <Image
        style={{
          height: 50,
          width: 50,
          borderRadius: 50,
          marginRight: 10,
        }}
        source={
          user.avatar ? {uri: user.avatar}  : require("../../assets/images/avatar.png")
        }
      />
      <Text style={[styleGlobal.text, { color: "#acacac" }]}>Thông báo nội dung với lớp học ở đây!</Text>
    </TouchableOpacity>
  );
};

export default ModalStatus;
