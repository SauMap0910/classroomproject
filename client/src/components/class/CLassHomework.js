import { View, Text, TouchableOpacity } from "react-native";
import React from "react";
import { MaterialIcons, Ionicons } from "@expo/vector-icons";
import styleGlobal from "../../assets/Style";
import { useState, useEffect } from "react";
import moment from "moment";
import { useSelector } from "react-redux";
const CLassHomework = ({post, navigate}) => {
  const {user, host} = useSelector(state => state)
  return (
    <TouchableOpacity
      style={{
        flexDirection: "row",
        alignItems: "center",
        padding: 5,
        borderWidth: 1,
        borderRadius: 15,
        borderColor: "#e4e6e9",
        marginVertical: 5,
      }}
      key={post.id}
      onPress={() => {
       user.id === host.id ? navigate('TeacherHomework', {post}) : navigate('DetailHomeWork', {post})
      }}
    >
      <MaterialIcons
        name="content-paste"
        size={30}
        color="white"
        style={{
          backgroundColor: "#64cbd8",
          padding: 9,
          borderRadius: 50,
          alignSelf: "center",
          marginRight: 10,
        }}
      />
      <View>
        <Text style={styleGlobal.text}>{`${post.displayName} đã đăng 1 bài tập mới`}</Text>
        <Text>{moment(post.createdDate).fromNow()}</Text>
      </View>
      <View style={{ flex: 1 }} />
      <Ionicons name="ellipsis-vertical-outline" size={24} color="black" />
    </TouchableOpacity>
  );
};

export default CLassHomework;
