import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  StatusBar,
} from "react-native";
import * as actionCreator from "../../redux/actionCreators";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import styleGlobal from "../../assets/Style";

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;

const PopupNoti = () => {
  const dispatch = useDispatch();
  const { changePopupNoti } = useSelector((state) => state);
  const close = () => {
    dispatch(actionCreator.actionChangePopupNoti(""));
  };

  return changePopupNoti !== "" ? (
    <TouchableOpacity
      onPress={close}
      activeOpacity={1}
      style={{
        width,
        height,
        justifyContent: "center",
        backgroundColor: "#4a4768",
        opacity: 0.9,
        marginTop: StatusBar.currentHeight,
        zIndex: 1000,
      }}
    >
      <TouchableOpacity
        activeOpacity={1}
        style={{
          backgroundColor: "#fff",
          margin: 20,
          borderRadius: 6,
          overflow: "hidden",
        }}
      >
        <Text
          style={[
            styleGlobal.text,
            {
              backgroundColor: "black",
              padding: 10,
              paddingVertical: 10,
              textAlign: "center",
              fontSize: 24,
            },
          ]}
        >
          Thông báo
        </Text>
        <Text
          style={[
            styleGlobal.text,
            { padding: 10, marginTop: 20, textAlign: "center", color: "black" },
          ]}
        >
          {changePopupNoti}
        </Text>
        <TouchableOpacity onPress={close} style={{ margin: 10, marginTop: 20 }}>
          <Text
            style={[
              styleGlobal.buttonText,
              {
                backgroundColor: "black",
                borderRadius: 5,
                paddingVertical: 5,
              },
            ]}
          >
            ĐÓNG
          </Text>
        </TouchableOpacity>
      </TouchableOpacity>
    </TouchableOpacity>
  ) : null;
};

export default PopupNoti;
