import { View, Text } from "react-native";
import React from "react";
import PopupNoti from "./PopupNoti";

const Popup = () => {
  return (
    <View style={{ position: "absolute" }}>
      <PopupNoti />
    </View>
  );
};

export default Popup;
