import { View, Image, TouchableOpacity } from "react-native";
import React from "react";
import { AntDesign } from "@expo/vector-icons";
const Head = () => {
  return (
    <View
      style={{
        width: "100%",
        height: 70,
        flexDirection: "row",
        justifyContent: "center",
      }}
    >
      <TouchableOpacity
        style={{
          padding: 10,
          position: "absolute",
          left: 0,
        }}
      >
        <AntDesign
          name="left"
          size={30}
          style={{
            color: "#303733",
          }}
        />
      </TouchableOpacity>
      <Image
        source={require("../assets/images/anh.jpg")}
        style={{ height: 60, width: 60, borderRadius: 50 }}
      />
    </View>
  );
};

export default Head;
