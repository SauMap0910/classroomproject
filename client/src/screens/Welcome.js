import {
  View,
  Text,
  ImageBackground,
  Dimensions,
  SafeAreaView,
  StyleSheet,
  Image,
  TouchableOpacity,
} from "react-native";
import React from "react";
import { AntDesign } from "@expo/vector-icons";
import * as Progress from "react-native-progress";
import { useState } from "react";
import { useEffect } from "react";
import { Ionicons } from "@expo/vector-icons";
import styleGlobal from "../assets/Style";
import { useSelector } from "react-redux";
const Welcome = ({ navigation }) => {
  const { token } = useSelector((state) => state);
  const [progress, SetProgess] = useState(0);
  const [load, setLoad] = useState(true);

  useEffect(() => {
    SetProgess(1);
    setTimeout(() => {
      setLoad(false);
    }, 2500);
  }, []);

  const { navigate } = navigation;
  return (
    <View
      style={[
        styleGlobal.container,
        { justifyContent: "center", backgroundColor: "white" },
      ]}
    >
      {load ? (
        <ImageBackground
          source={require("../assets/images/img_bg.jpg")}
          style={{ justifyContent: "center" }}
        >
          <SafeAreaView style={{ alignItems: "center" }}>
            <Ionicons name="book-outline" size={150} color="black" />
            <Progress.Bar
              style={styles.margin}
              progress={progress}
              width={150}
              color={"black"}
              height={10}
              borderRadius={10}
              animationConfig={{ bounciness: 20 }}
            />
            <Text style={styleGlobal.textTitle}>NHÓM 2</Text>
          </SafeAreaView>
        </ImageBackground>
      ) : (
        <View style={[styleGlobal.container, { backgroundColor: "white" }]}>
          <View style={{ marginVertical: 40 }}>
            <Image
              source={require("../assets/images/anh1.jpg")}
              style={{ height: 300, width: Dimensions.get("window").width }}
            />
          </View>
          <View style={{ marginHorizontal: 40 }}>
            <Text
              style={[
                styleGlobal.textTitle,
                { textAlign: "center", marginBottom: 25 },
              ]}
            >
              Bắt đầu việc học tập của bạn.
            </Text>
            <Text
              style={[
                styleGlobal.textThin,
                { textAlign: "center", marginHorizontal: 20 },
              ]}
            >
              Ứng dụng khiến việc học tập của bạn trở nên dễ dàng hơn
            </Text>
          </View>

          <View
            style={{
              position: "absolute",
              bottom: "10%",
              alignSelf: "center",
            }}
          >
            <TouchableOpacity
              style={{
                flexDirection: "row",
                backgroundColor: "black",
                padding: 20,
                borderRadius: 50,
              }}
              onPress={() => {
                token === ""
                  ? navigate("Login")
                  : navigate("UiTab", { screen: "Classes" });
              }}
            >
              <AntDesign
                name="arrowright"
                size={24}
                color="black"
                style={{
                  backgroundColor: "white",
                  padding: 10,
                  borderRadius: 50,
                }}
              />
              <Text
                style={[
                  styleGlobal.text,
                  {
                    color: "white",
                    padding: 10,
                    marginHorizontal: 10,
                  },
                ]}
              >
                Bắt đầu
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  margin: {
    marginVertical: 10,
  },
  h1: {
    fontSize: 32,
    fontWeight: "500",
    color: "black",
  },
});

export default Welcome;
