import { View, SafeAreaView, Text, Image, FlatList, TouchableOpacity } from 'react-native'
import React, { useState } from 'react'
import Header from '../components/Header'
import styleGlobal from '../assets/Style'
import { useDispatch, useSelector } from 'react-redux'
import { API } from '../Config'
import { api_get, api_put, URL_API } from '../helper/Api'
import moment from "moment";
import { useEffect } from 'react'
import { actionChangePopupNoti, ActionSaveHomework } from '../redux/actionCreators'
import { ScrollView, TextInput } from 'react-native-gesture-handler'
const TeacherHomework = ({ route }) => {
    const { detailClass, classStudent, homeworks, token, host } = useSelector(state => state)
    const { post } = route.params
    const dispatch = useDispatch()
    const [homework, setHomework] = useState([])
    const [point, setPoint] = useState(0)
    const [key, setKey] = useState(0)
    const getHomework = () => {
        let request = API + URL_API.GET_HOMEWORK + `?postId=${post.id}`
        api_get(dispatch, request, token, (res) => {
            dispatch(ActionSaveHomework(res.data))
        })
    }
    useEffect(() => {
        getHomework()
    }, [post])

    

    useEffect(() => {
        const result = homeworks.filter((e) => (e.userId !== host.id && e.filePath))
        setHomework(result)
    }, [homeworks])

    const chamdiem = (id) => {
        const request = API + URL_API.SUBMIT_POINT + `?id=${id}&point=${point}`
        api_put(dispatch, request, {}, (res) => {
            getHomework()
        });
    }

    return (
        <ScrollView style={[styleGlobal.container]}>
            <View style={{ backgroundColor: '#C9C9C9', height: 38 }} >
                <Header />
                <Text style={[{ textAlign: 'center', lineHeight: 38 }, styleGlobal.text]}>Lớp: {detailClass.className}</Text>
            </View>
            <View
                style={{
                    padding: 5,
                    borderWidth: 1,
                    borderRadius: 15,
                    borderColor: "#e4e6e9",
                    marginVertical: 5,
                }} key={post.id}
            >
                <View style={{ flexDirection: "row", alignItems: "center", borderBottomColor: '#C9C9C9', borderBottomWidth: 1, marginBottom: 10 }}>
                    <Image
                        style={{
                            height: 50,
                            width: 50,
                            borderRadius: 50,
                            marginRight: 10,
                        }}
                        source={post.avatar ? { uri: post.avatar } : require('../assets/images/avatar.png')}
                    />
                    <View>
                        <Text style={[styleGlobal.text, { color: "black" }]}>
                            {post.displayName}
                        </Text>
                        <Text style={{ fontSize: 14, fontWeight: '400' }}>Đã đăng bài tập vào lúc {moment(post.createdDate).format('DD/MM/YYYY')}</Text>
                        <View style={{ flexDirection: 'row', marginVertical: 5 }}>
                            <Text style={{ fontWeight: '600' }}>Đến hạn 23:59, {moment(post.expireDate).format('DD/MM/YYYY')} </Text>
                        </View>
                    </View>

                </View>

                <Text style={{ marginVertical: 5, marginBottom: 10 }}>{post.body}</Text>
                {(post?.title.slice(post?.title.indexOf('.') + 1) !== 'jpg' && post?.title != 1) ?

                    <TouchableOpacity style={{ flexDirection: 'row', borderColor: '#e9eaea', borderWidth: 1 }}>
                        <View style={{ padding: 10, borderRightWidth: 1, borderColor: '#e9eaea' }}>
                            <Image style={{ width: 40, height: 40 }} source={require('../assets/images/google-drive.png')} />
                        </View>
                        <View style={{ borderColor: '#e9eaea', padding: 10, }}>
                            <Text>{post?.title}</Text>
                            <Text style={{
                                textTransform: 'uppercase'
                            }}>{post?.title.slice(post?.title.indexOf('.') + 1)}</Text>

                        </View>
                    </TouchableOpacity>
                    : post?.topic != 1 ?
                        <Image
                            style={{
                                margin: 1,
                                borderWidth: 1,
                                borderRadius: 10,
                                width: "100%",
                                height: undefined,
                                aspectRatio: 1 / 1,
                            }}
                            resizeMode="stretch"
                            source={{ uri: post.topic }}
                        /> : null

                }

            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                <View style={{ alignItems: 'center' }}>
                    <Text style={{ fontSize: 26, fontWeight: '500' }}>{homework.length}</Text>
                    <Text style={{ fontSize: 10, color: "gray" }}>Đã nộp</Text>
                </View>
                <View style={{ borderLeftColor: 'black', borderLeftWidth: 1, marginHorizontal: 20 }} />
                <View style={{ alignItems: 'center' }}>
                    <Text style={{ fontSize: 26, fontWeight: '500' }}>{classStudent.length - 1}</Text>
                    <Text style={{ fontSize: 10, color: 'gray' }}>Đã giao</Text>
                </View>
            </View>

            <View>
                <FlatList
                    data={homework}
                    renderItem={({ item, index }) => {
                        return (
                            <View style={{ borderColor: '#e9eaea', borderWidth: 1, marginTop: 20, marginHorizontal: 20 }}>
                            <View style={{ padding: 10, borderRightWidth: 1, borderColor: '#e9eaea', alignItems: 'center' }}>
                                <Image style={{ width: 110, height: 110 }} source={{ uri: item.avatar }} />
                                <Text style={{ marginTop: 5 }}>{item.displayname}</Text>
                                <View>
                                    <TextInput keyboardType='numeric'  placeholder={item.point ? `${item.point}` : "Điểm"} onFocus={() => {
                                        setKey(index)
                                    }} onChangeText={(e) => {
                                        if(index == key) {
                                            if(e > 100) {
                                                dispatch(actionChangePopupNoti('Max là 100 điểm má ơi!'))
                                            } else {
                                                setPoint(e)
                                            }
                                            
                                        }
                                    }} onBlur={() => {
                                        if(index == key && point <= 100) {
                                            chamdiem(item.id)
                                        } else {
                                            return
                                        }
                                    }}/><Text>/100</Text>
                                </View>
                            </View>
                            <View style={{ borderColor: '#e9eaea', flexDirection: 'row' }}>
                                <View style={{ padding: 5, borderRightWidth: 1, borderTopWidth: 1, borderColor: '#e9eaea' }}>
                                    <Image style={{ width: 40, height: 40 }} source={require('../assets/images/google-drive.png')} />
                                </View>
                                <View style={{ borderColor: '#e9eaea', padding: 10, borderTopWidth: 1, borderTopColor: '#e9eaea' }}>
                                    <Text style={{ fontSize: 12 }}>{item?.fileName}</Text>
                                    <Text style={{
                                        textTransform: 'uppercase'
                                    }}>{item?.fileName.slice(item?.fileName.indexOf('.') + 1)}</Text>
                                </View>
                            </View>
                        </View>
                        )
                    }}
                    //Setting the number of column
                    numColumns={2}
                    keyExtractor={(item, index) => index}
                />
            </View>
        </ScrollView>
    )
}

export default TeacherHomework