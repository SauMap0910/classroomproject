import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  Switch,
} from "react-native";
import React, { useState } from "react";
import styleGlobal from "../assets/Style";
import Head from "../components/Head";
import { AntDesign } from "@expo/vector-icons";
import { MaterialIcons, FontAwesome5, Ionicons } from "@expo/vector-icons";
import ModalLogout from "../components/myComponents/ModalLogout";
import { logout } from "../redux/actionCreators";
import { useDispatch } from "react-redux";

const Setting = ({ navigation }) => {
  const dispatch = useDispatch()
  const [isEnabledLockApp, setEnabledLockApp] = useState(true);
  const [visible, setVisible] = useState(false);
  const { navigate } = navigation;
  const showModal = () => {
    setVisible(true);
  };

  const hideModal = () => {
    setVisible(false);
  };
  return (
    <SafeAreaView style={[styleGlobal.container, { backgroundColor: "white" }]}>
      <Head />
      <Text
        style={{
          marginHorizontal: 20,
          marginVertical: 40,
          fontSize: 32,
          fontWeight: "400",
          color: "#4e4e5c",
        }}
      >
        Setting
      </Text>

      <View style={{ marginHorizontal: 20 }}>
        <TouchableOpacity style={styles.view}>
          <MaterialIcons
            name="notifications-on"
            size={24}
            color="white"
            style={[styles.icon, { backgroundColor: "#fc96f5" }]}
          />
          <Text style={[styleGlobal.text, styles.text]}>Thông báo</Text>
          <View style={{ flex: 1 }} />
          <AntDesign name="right" size={24} color="#e6e6e6" />
        </TouchableOpacity>
      </View>
      <View style={{ marginHorizontal: 20 }}>
        <TouchableOpacity style={styles.view}>
          <MaterialIcons
            name="privacy-tip"
            size={24}
            color="white"
            style={[styles.icon, { backgroundColor: "#c0cafe" }]}
          />
          <Text style={[styleGlobal.text, styles.text]}>Bảo mật</Text>
          <View style={{ flex: 1 }} />
          <AntDesign name="right" size={24} color="#e6e6e6" />
        </TouchableOpacity>
      </View>
      <View style={{ marginHorizontal: 20 }}>
        <TouchableOpacity style={styles.view}>
          <AntDesign
            name="question"
            size={24}
            color="white"
            style={[styles.icon, { backgroundColor: "#8fcffd" }]}
          />
          <Text style={[styleGlobal.text, styles.text]}>Trung tâm hỗ trợ</Text>
          <View style={{ flex: 1 }} />
          <AntDesign name="right" size={24} color="#e6e6e6" />
        </TouchableOpacity>
      </View>
      <View style={{ marginHorizontal: 20 }}>
        <TouchableOpacity style={styles.view}>
          <FontAwesome5
            name="dot-circle"
            size={24}
            color="white"
            style={[styles.icon, { backgroundColor: "#aaf09a" }]}
          />
          <Text style={[styleGlobal.text, styles.text]}>Cài đặt chung</Text>
          <View style={{ flex: 1 }} />
          <AntDesign name="right" size={24} color="#e6e6e6" />
        </TouchableOpacity>
      </View>
      <View style={{ marginHorizontal: 20 }}>
        <TouchableOpacity style={styles.view}>
          <AntDesign
            name="info"
            size={24}
            color="white"
            style={[styles.icon, { backgroundColor: "#feda9f" }]}
          />
          <Text style={[styleGlobal.text, styles.text]}>Về chúng tôi</Text>
          <View style={{ flex: 1 }} />
          <AntDesign name="right" size={24} color="#e6e6e6" />
        </TouchableOpacity>
      </View>
      <View
        style={{
          flexDirection: "row",
          marginHorizontal: 20,
          alignItems: "center",
          marginBottom: 20,
        }}
      >
        <Ionicons
          name="finger-print-sharp"
          size={24}
          color="white"
          style={[styles.icon, { backgroundColor: "#feb8c0" }]}
        />

        <Text style={[styleGlobal.text, styles.text]}>
          Khóa ứng dụng vân tay
        </Text>
        <View style={{ flex: 1 }} />
        <Switch
          trackColor={{ false: "#c9c9c9", true: "#5cb85c" }}
          thumbColor={"#54aed8"}
          //ios_backgroundColor="#3e3e3e"
          onValueChange={() => {
            setEnabledLockApp(!isEnabledLockApp);
          }}
          value={isEnabledLockApp}
        />
      </View>
      <View style={{ marginHorizontal: 20 }}>
        <TouchableOpacity style={styles.view} onPress={showModal}>
          <AntDesign
            name="logout"
            size={24}
            color="white"
            style={[styles.icon, { backgroundColor: "#c9c9c9" }]}
          />
          <Text style={[styleGlobal.text, styles.text]}>Đăng xuất</Text>
          <View style={{ flex: 1 }} />
          <AntDesign name="right" size={24} color="#e6e6e6" />
        </TouchableOpacity>
      </View>
      <ModalLogout visible={visible} back={hideModal} title={'Đăng xuất?'} content={'Bạn có chắc rằng mình muốn đăng xuất!'} cf={'Đăng xuất'}   onPress={() => {
                let keys = ["token", "user"];
                dispatch(logout(keys));
                navigate("Welcome");
              }} />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  view: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 20,
  },
  icon: {
    padding: 10,
    borderRadius: 50,
  },
  text: {
    marginHorizontal: 30,
    fontSize: 18,
  },
  linearGradient: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    height: 200,
    width: 350,
  },
});

export default Setting;
