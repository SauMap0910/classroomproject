import { View, SafeAreaView, StyleSheet, TouchableOpacity } from "react-native";
import React from "react";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import styleGlobal from "../assets/Style";
import Head from "../components/Head";
import { MaterialIcons } from "@expo/vector-icons";
import { MaterialCommunityIcons, FontAwesome5 } from "@expo/vector-icons";
import { Ionicons } from "@expo/vector-icons";
import {
  Avatar,
  Title,
  Caption,
  Text,
  TouchableRipple,
} from "react-native-paper";
import ModalProfile from "../components/myComponents/ModalProfile";
import { useDispatch, useSelector } from "react-redux";
import { ActionshowEditUser } from "../redux/actionCreators";
import Class from "./Class";

const Profile = () => {
  const dispatch = useDispatch();
  const { user,CLass } = useSelector((state) => state);
  return (
    <SafeAreaView style={styleGlobal.container}>
      <Head />
      <Text
        style={{
          marginHorizontal: 20,
          marginVertical: 30,
          fontSize: 32,
          fontWeight: "400",
          color: "#4e4e5c",
        }}
      >
        Hồ sơ
      </Text>
      <View style={[styles.userInfoSection]}>
        <View style={{ flexDirection: "row" }}>
          { user.avatar ? 
            <Avatar.Image source={{ uri: user.avatar }} size={80} /> :
          <Avatar.Image source={require('../assets/images/avatar.png')} size={80} />
          }
          
          <View style={{ marginLeft: 20 }}>
            <Title
              style={[
                styles.title,
                {
                  marginTop: 15,
                  marginBottom: 5,
                },
              ]}
            >
              {user.username}
            </Title>
            <Caption style={styles.caption}>{`@${user.displayname}`}</Caption>
          </View>
        </View>
      </View>
      {/* <MaterialCommunityIcons name="identifier" size={24} color="black" />*/}
      <View style={styles.userInfoSection}>
        <View style={styles.row}>
          <Ionicons name="school" size={20} color="#777777" />
          <Text style={{ color: "#777777", marginLeft: 20 }}>HUCE, Hà Nội</Text>
        </View>
        <View style={styles.row}>
          <MaterialIcons name="computer" color="#777777" size={20} />
          <Text style={{ color: "#777777", marginLeft: 20 }}>
            Khoa học máy tính
          </Text>
        </View>
        <View style={styles.row}>
          <MaterialCommunityIcons name="identifier" size={20} color="#777777" />
          <Text style={{ color: "#777777", marginLeft: 20 }}>12364</Text>
        </View>
        <View style={styles.row}>
          <Icon name="phone" color="#777777" size={20} />
          <Text style={{ color: "#777777", marginLeft: 20 }}>
            +84 012345678
          </Text>
        </View>
        <View style={styles.row}>
          <Icon name="email" color="#777777" size={20} />
          <Text style={{ color: "#777777", marginLeft: 20 }}>{user.email}</Text>
        </View>
      </View>

      <View style={styles.infoBoxWrapper}>
        <View
          style={[
            styles.infoBox,
            {
              borderRightColor: "#dddddd",
              borderRightWidth: 1,
            },
          ]}
        >
          <Title>{Class.length}</Title>
          <Caption>Lớp</Caption>
        </View>
        <View style={styles.infoBox}>
          <Title>0</Title>
          <Caption>Bài tập</Caption>
        </View>
      </View>

      <View style={styles.menuWrapper}>
        <TouchableRipple
          onPress={() => {
            dispatch(ActionshowEditUser(true));
          }}
        >
          <View style={styles.menuItem}>
            <FontAwesome5 name="user-edit" size={24} color="#54aed8" />
            <Text style={styles.menuItemText}>Cập nhật hồ sơ</Text>
          </View>
        </TouchableRipple>
      </View>

      <ModalProfile />
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  userInfoSection: {
    paddingHorizontal: 30,
    marginBottom: 25,
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
    fontWeight: "500",
  },
  row: {
    flexDirection: "row",
    marginBottom: 10,
  },
  infoBoxWrapper: {
    borderBottomColor: "#dddddd",
    borderBottomWidth: 1,
    borderTopColor: "#dddddd",
    borderTopWidth: 1,
    flexDirection: "row",
    height: 100,
  },
  infoBox: {
    width: "50%",
    alignItems: "center",
    justifyContent: "center",
  },
  menuWrapper: {
    marginTop: 10,
  },
  menuItem: {
    flexDirection: "row",
    paddingVertical: 15,
    paddingHorizontal: 30,
  },
  menuItemText: {
    color: "#777777",
    marginLeft: 20,
    fontWeight: "600",
    fontSize: 16,
    lineHeight: 26,
  },
});

export default Profile;
