import { View, SafeAreaView, Text, Image, TextInput, TouchableOpacity } from 'react-native'
import React, { useEffect, useState } from 'react'
import styleGlobal from '../assets/Style'
import Header from '../components/Header'
import { useDispatch, useSelector } from 'react-redux'
import { Feather, AntDesign } from "@expo/vector-icons";
import { MaterialIcons, Ionicons } from "@expo/vector-icons";
import moment from "moment";
import * as DocumentPicker from 'expo-document-picker';
import { API } from '../Config'
import { api_get, api_put, URL_API } from '../helper/Api'
import { ActionSaveHomework } from '../redux/actionCreators'
const DetailHomeWork = ({ route }) => {
    const { detailClass, homeworks, token, user } = useSelector(state => state)
    const { post } = route.params
    const [file, SetFile] = useState()
    const [Homework, setHomework] = useState({})

    useEffect(() => {
        const result = homeworks.filter((e) => e.userId === user.id && e.filePath)
        console.log('result', result)
        setHomework(result[0])
    }, [homeworks, post])

    console.log('a',homeworks)
    const dispatch = useDispatch()
    const pickFile = async () => {
        let result = await DocumentPicker.getDocumentAsync({})
        SetFile(result)
    }
    const getHomework = () => {
        let request = API + URL_API.GET_HOMEWORK + `?postId=${post.id}`
        api_get(dispatch, request, token, (res) => {
            dispatch(ActionSaveHomework(res.data))
        })
    }
    useEffect(() => {
        getHomework()
    }, [post])

    const handleSubmit = () => {
        let request = API + URL_API.SUBMIT_HOMEWORK + `?userId=${user.id}&postId=${post.id}&filepath=${file.uri}&filename=${file.name}&mimetype=${file.mimeType}`;
        api_put(dispatch, request, {}, (res) => {
            console.log('res', res)
            getHomework()
        });
    }

    const handleDelete = () => {
        SetFile()
        let request = API + URL_API.SUBMIT_HOMEWORK + `?userId=${user.id}&postId=${post.id}&filepath=&filename=&mimetype=`;
        api_put(dispatch, request, {}, (res) => {
            console.log('res', res)
            getHomework()
        });
    }

    console.log('Homework',Homework)
    return (
        <SafeAreaView style={[styleGlobal.container]}>
            <View style={{ backgroundColor: '#C9C9C9', height: 38 }} >
                <Header />
                <Text style={[{ textAlign: 'center', lineHeight: 38 }, styleGlobal.text]}>Lớp: {detailClass.className}</Text>
            </View>
            <View
                style={{
                    padding: 5,
                    borderWidth: 1,
                    borderRadius: 15,
                    borderColor: "#e4e6e9",
                    marginVertical: 5,
                }} key={post.id}
            >
                <View style={{ flexDirection: "row", alignItems: "center", borderBottomColor: '#C9C9C9', borderBottomWidth: 1, marginBottom: 10 }}>
                    <Image
                        style={{
                            height: 50,
                            width: 50,
                            borderRadius: 50,
                            marginRight: 10,
                        }}
                        source={post.avatar ? { uri: post.avatar } : require('../assets/images/avatar.png')}
                    />
                    <View>
                        <Text style={[styleGlobal.text, { color: "black" }]}>
                            {post.displayName}
                        </Text>
                        <Text style={{ fontSize: 14, fontWeight: '400' }}>Đã đăng bài tập vào lúc {moment(post.createdDate).format('DD/MM/YYYY')}</Text>
                        <View style={{ flexDirection: 'row', marginVertical: 5 }}>
                            <Text style={{ marginRight: 60, fontWeight: '600' }}>0/100 điểm</Text>
                            <Text style={{ fontWeight: '600' }}>Đến hạn 23:59, {moment(post.expireDate).format('DD/MM/YYYY')} </Text>
                        </View>
                    </View>

                </View>

                <Text style={{ marginVertical: 5, marginBottom: 10 }}>{post.body}</Text>
                {(post?.title.slice(post?.title.indexOf('.') + 1) !== 'jpg' && post?.title != 1) ?

                    <TouchableOpacity style={{ flexDirection: 'row', borderColor: '#e9eaea', borderWidth: 1 }}>
                        <View style={{ padding: 10, borderRightWidth: 1, borderColor: '#e9eaea' }}>
                            <Image style={{ width: 40, height: 40 }} source={require('../assets/images/google-drive.png')} />
                        </View>
                        <View style={{ borderColor: '#e9eaea', padding: 10, }}>
                            <Text>{post?.title}</Text>
                            <Text style={{
                                textTransform: 'uppercase'
                            }}>{post?.title.slice(post?.title.indexOf('.') + 1)}</Text>

                        </View>
                    </TouchableOpacity>
                    : post?.topic != 1 ?
                        <Image
                            style={{
                                margin: 1,
                                borderWidth: 1,
                                borderRadius: 10,
                                width: "100%",
                                height: undefined,
                                aspectRatio: 1 / 1,
                            }}
                            resizeMode="stretch"
                            source={{ uri: post.topic }}
                        /> : null

                }

                <View
                    style={{
                        flexDirection: "row",
                        alignItems: "center",
                        borderTopWidth: 1,
                        borderColor: "#dadce0",
                        padding: 10,
                        justifyContent: "space-around",
                    }}
                >
                    <Image
                        style={{
                            height: 40,
                            width: 40,
                            borderRadius: 50,
                            marginRight: 10,
                        }}
                        source={post.avatar ? { uri: post.avatar } : require("../assets/images/avatar.png")}
                    />


                    <View
                        style={{
                            flexDirection: "row",
                            borderRadius: 15,
                            borderWidth: 1,
                            alignSelf: "center",
                            borderColor: "#dadce0",
                        }}
                    >
                        <TextInput
                            placeholder="Thêm nhận xét"
                            style={{
                                maxWidth: "100%",
                                minWidth: "65%",
                                padding: 6,
                            }}
                            multiline={true}
                            scrollEnabled={false}
                        />

                        <TouchableOpacity style={{ padding: 10 }}>
                            <Feather name="send" size={24} color="#dadce0" />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>

            <View style={{ margin: 10 }}>
                <View style={{flexDirection:'row'}}> 
                <Text style={{ fontSize: 24, fontWeight: '500' }}>Bài tập của bạn</Text>
                <View style={{flex:1}}/>
                <Text style={{fontSize:12, color: Homework ? 'green' : 'red'}}> {Homework ? 'Đã nộp bài' : 'Chưa nộp bài'}</Text>
                </View>
                {!file ? <TouchableOpacity style={{ padding: 10 }} onPress={pickFile}>
                    <AntDesign name="addfile" size={24} color="black" />
                </TouchableOpacity> : <View style={{ flexDirection: 'row', borderColor: '#e9eaea', borderWidth: 1 }}>
                    <View style={{ padding: 10, borderRightWidth: 1, borderColor: '#e9eaea' }}>
                        <Image style={{ width: 40, height: 40 }} source={file.mimeType === 'image/jpeg' ? { uri: file.uri } : require('../assets/images/google-drive.png')} />
                    </View>
                    <View style={{ borderColor: '#e9eaea', padding: 10, width: '83%' }}>
                        <TouchableOpacity style={{ position: 'absolute', top: 0, right: 0 }} onPress={() => {
                           Homework ? handleDelete() : SetFile()
                        }}>
                            <Ionicons name="close" size={24} color="black" />
                        </TouchableOpacity>
                        <Text style={{ fontSize: 12 }}>{file?.name}</Text>
                        <Text style={{
                            textTransform: 'uppercase'
                        }}>{file?.name.slice(file?.name.indexOf('.') + 1)}</Text>

                    </View>
                </View>}
                {
                    file &&
                    <View style={{ alignItems: 'center', marginVertical: 10 }}>
                        <TouchableOpacity onPress={handleSubmit} style={{ padding: 10, backgroundColor: '#64cbd8', width: '30%', borderRadius: 50 }}>
                            <Text style={{ textAlign: 'center' }}>Nộp bài</Text>
                        </TouchableOpacity>
                    </View>
                }


            </View>


        </SafeAreaView>
    )
}

export default DetailHomeWork