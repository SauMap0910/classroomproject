import {
  View,
  Text,
  Image,
  SafeAreaView,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import React, { useState } from "react";
import { AntDesign, FontAwesome5 } from "@expo/vector-icons";
import { useTogglePasswordVisibility } from "../helper/hidePass";
import { useDispatch, useSelector } from "react-redux";
import styleGlobal from "../assets/Style";
import { WIDTH } from "../Const";
import {
  actionSaveToken,
  actionSaveUser,
  getAlert,
} from "../redux/actionCreators";
import { validLogin } from "../helper/valid";
import { api_login } from "../helper/Api";
import Header from "../components/Header";
import { useEffect } from "react";

const Login = ({ navigation }) => {
  const dispatch = useDispatch();
  const { alert, showLoading } = useSelector((state) => state);
  const { navigate } = navigation;
  const { hidePass, rightIconPass, handlePassword } =
    useTogglePasswordVisibility();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const validateLogin = () => {
    const check = validLogin(username, password);
    if (check.errLength > 0) {
      dispatch(getAlert(check.errMsg));
      setTimeout(() => {
        dispatch(getAlert({}));
      }, 2000);
    } else {
      login(username, password);
    }
  };

  const login = (username, password) => {
    const data = { username, password };
    api_login(dispatch, data, (res) => {
      dispatch(actionSaveUser(res.userData));
      dispatch(actionSaveToken(res.token));
      navigate("UiTab", { screen: "Classes" });
    });
  };

  return (
    <SafeAreaView style={[styleGlobal.container, { backgroundColor: "white" }]}>
      <KeyboardAwareScrollView>
        <View style={{ marginHorizontal: 20, flex: 1 }}>
          <Header />
          <Image
            source={require("../assets/images/anh2.jpg")}
            style={{
              height: 300,
              width: WIDTH - 40,
            }}
          />
          <Text
            style={{
              fontSize: 32,
              fontWeight: "500",
              marginBottom: 10,
            }}
          >
            Đăng nhập
          </Text>

          <View style={styles.searchSection}>
            <AntDesign
              name="user"
              size={24}
              color="black"
              style={styles.searchIcon}
            />
            <TextInput
              style={styles.input}
              placeholder="Tên đăng nhập"
              value={username}
              onChangeText={(value) => setUsername(value)}
            />
          </View>
          <Text style={styles.textErr}>
            {alert.username ? alert.username : ""}
          </Text>

          <View style={styles.searchSection}>
            <AntDesign
              name="lock"
              size={24}
              color="black"
              style={styles.searchIcon}
            />
            <TextInput
              style={styles.input}
              placeholder="Mật khẩu"
              secureTextEntry={hidePass}
              value={password}
              onChangeText={(value) => setPassword(value)}
            />

            <TouchableOpacity
              onPress={handlePassword}
              style={{ alignSelf: "center", position: "absolute", right: 0 }}
            >
              <FontAwesome5 name={rightIconPass} size={24} color="black" />
            </TouchableOpacity>
          </View>
          <Text style={styles.textErr}>
            {alert.password ? alert.password : ""}
          </Text>

          <Text
            style={{ textAlign: "right", marginVertical: 10, color: "#0065ff" }}
          >
            Quên mật khẩu?
          </Text>
          <TouchableOpacity
            style={{
              backgroundColor: "black",
              padding: 15,
              borderRadius: 20,
            }}
            onPress={validateLogin}
          >
            <Text
              style={[styles.text, { color: "white", textAlign: "center" }]}
            >
              Đăng nhập
            </Text>
          </TouchableOpacity>

          <View style={{ flexDirection: "row", marginVertical: 10 }}>
            <View
              style={{
                flexGrow: 1,
                flexShrink: 1,
                height: 1,
                backgroundColor: "#DADEE3",
                margin: 13,
              }}
            />
            <Text
              style={{
                color: "#808080",
                fontSize: 18,
                textAlign: "center",
              }}
            >
              HOẶC
            </Text>
            <View
              style={{
                flexGrow: 1,
                flexShrink: 1,
                height: 1,
                backgroundColor: "#DADEE3",
                margin: 13,
              }}
            />
          </View>
          <TouchableOpacity
            style={{
              flexDirection: "row",
              justifyContent: "center",
              backgroundColor: "#f1f5f6",
              padding: 15,
              borderRadius: 20,
            }}
          >
            <AntDesign name="google" size={24} color="#D9453D" />
            <Text
              style={[
                styles.text,
                { color: "black", marginHorizontal: 20, fontWeight: "300" },
              ]}
            >
              Đăng nhập bằng Google
            </Text>
          </TouchableOpacity>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              marginVertical: 10,
            }}
          >
            <Text style={{ fontSize: 16 }}>Thành viên mới ? </Text>
            <TouchableOpacity
              onPress={() => {
                navigate("Register");
              }}
            >
              <Text style={{ color: "#0065ff", fontSize: 16 }}>Đăng ký</Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  searchSection: {
    marginVertical: 5,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
  },
  searchIcon: {
    padding: 20,
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: "#fff",
    color: "#424242",
    borderBottomColor: "#DADEE3",
    borderBottomWidth: 1,
  },
  textErr: {
    color: "red",
    marginLeft: "18%",
  },
});

export default Login;
