import {
  View,
  Text,
  SafeAreaView,
  Image,
  StyleSheet,
  TouchableOpacity,
  Keyboard,
  Dimensions,
  Pressable,
  TextInput
} from "react-native";
import {
  actions,
  RichEditor,
  RichToolbar,
} from "react-native-pell-rich-editor";
import { AntDesign } from "@expo/vector-icons";
import React, { useRef, useState, useEffect } from "react";
import styleGlobal from "../assets/Style";
import Header from "../components/Header";
import { useDispatch, useSelector } from "react-redux";
import { TabView, SceneMap, TabBar } from "react-native-tab-view";
import { HEIGHT, WIDTH } from "../Const";
import ModalStatus from "../components/class/ModalStatus";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { actionSaveClass, ActionSaveClassStudent, ActionSaveDetailClass, ActionSaveHostClass, ActionSavePost, actionShowModal, ActionshowStatus, OnEdit } from "../redux/actionCreators";
import CLassHomework from "../components/class/CLassHomework";
import Status from "../components/class/Status";
import { MaterialIcons, Ionicons } from "@expo/vector-icons";
import { api_delete, api_get, api_post, api_post1, URL_API } from "../helper/Api";
import { API } from "../Config";
import SelectDropdown from 'react-native-select-dropdown'
import ModalLogout from "../components/myComponents/ModalLogout";
import ModalJoinClass from "../components/myComponents/ModalJoinClass";
import * as DocumentPicker from 'expo-document-picker';
import DateTimePicker from "@react-native-community/datetimepicker";
import moment from "moment";
import 'moment/locale/vi'
const FirstRoute = ({ navigate }) => {
  const { token, detailClass, host, user, posts } = useSelector((state) => state);
  const [file, SetFile] = useState()
  const [textDate, setTextDate] = useState('')
  const [body, setBody] = useState('')
  const [expireDate, setExpireDate] = useState(new Date(Date.now()))
  const [showDate, setShowDate] = useState(false)
  const [homework, setHomework] = useState(false)
  const [ImageHeight, setImageHeight] = useState(0);
  const [ImageWidth, setImageWidth] = useState(0);
  const screenWidth = Dimensions.get('screen').width;
  const maxWidth = screenWidth / 1000;
  const size = ImageWidth / ImageHeight;
  const richText = useRef();
  const { showStatus } = useSelector((state) => state);
  const dispatch = useDispatch();
  const pickFile = async () => {
    let result = await DocumentPicker.getDocumentAsync({})
    SetFile(result)
  }
  const currentDay = Date.now()
  const onchangeDate = (event, selectedDate) => {
    const currentDate = selectedDate || expireDate;
    let tempDate = new Date(currentDate);
    let fdate =
      tempDate.getFullYear() +
      "-" +
      (tempDate.getMonth() + 1) +
      "-" +
      tempDate.getDate();
    setShowDate(false)
    setExpireDate(currentDate)
    setTextDate(fdate)
  };

  const showMode = () => {
    setShowDate(true)
  }

  useEffect(() => {
    if (file?.mimeType === "image/jpeg") {
      Image.getSize(file.uri, (width, height) => {
        setImageHeight(height);
        setImageWidth(width);
      });
    }
  }, [file]);


  const getPost = () => {
    let request = API + URL_API.GET_POST + `?classId=${detailClass.id}`
    api_get(dispatch, request, token, (res) => {
      dispatch(ActionSavePost(res.data))
    })
  }

  const resetState = () => {
    dispatch(ActionshowStatus(false))
  }

  const uploadPost = () => {
    let request = API + URL_API.CREATE_POST;
    const newbody = body.replace(/<(.|\n)*?>/g, "").trim();
    const data = {
      Title: file?.name ? file?.name : "1",
      Body: newbody,
      Topic: file?.uri ? file?.uri : "1",
      ClassId: detailClass.id,
      IsHomeWork: homework,
      MaxPoint: 0,
      ExpireDate: textDate,
      Attachments: []
    };
    api_post1(dispatch, request, data, (res) => {
      let request = API + URL_API.GET_POST + `?classId=${detailClass.id}`
      api_get(dispatch, request, token, (res) => {
        dispatch(ActionSavePost(res.data))
        resetState()
      })
    });
  }

  useEffect(() => {
    getPost()
  }, [detailClass.id])

  const baitap = ['Có', 'Không']
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <KeyboardAwareScrollView>
        <View style={{ flex: 1, marginTop: 10, marginHorizontal: 20 }}>
          {showStatus ? (
            <View style={styles.richTextContainer}>
              <RichEditor
                disabled={false}
                ref={richText}
                style={styles.rich}
                placeholder={"Nhập vào đây!"}
                initialHeight={100}
                scrollEnabled={false}
                onChange={descriptionText => {
                  setBody(descriptionText)
                }}
              />
              <RichToolbar
                editor={richText}
                selectedIconTint="#873c1e"
                iconTint="#312921"
                actions={[
                  actions.insertImage,
                  actions.setBold,
                  actions.setItalic,
                  actions.insertBulletsList,
                  actions.insertOrderedList,

                ]}
                onPressAddImage={pickFile}
                style={styles.richTextToolbarStyle}
              />
              {user.id === host.id &&
                <SelectDropdown
                  data={baitap}
                  buttonStyle={styles.dropS}
                  buttonTextStyle={styles.textDrop}
                  rowTextStyle={styles.textDrop}
                  dropdownStyle={{
                    marginTop: -30,
                    borderRadius: 20
                  }}
                  onSelect={(selectedItem, index) => {
                    selectedItem === 'Có' ? setHomework(true) : setHomework(false)
                  }}
                  renderCustomizedButtonChild={(selectedItem, index) => {
                    return (
                      <View
                        style={{
                          flexDirection: "row",
                        }}
                      >
                        <Text style={styles.textdlabel}>Bài tập?</Text>
                        <View style={{ flex: 1 }} />
                        <Text style={{ marginRight: 10 }}>
                          {homework ? 'Có' : 'Không'}
                        </Text>
                      </View>
                    );
                  }}
                  buttonTextAfterSelection={(selectedItem, index) => {
                    return selectedItem;
                  }}
                  rowTextForSelection={(item, index) => {
                    return item;
                  }}
                  renderDropdownIcon={(isOpened) => {
                    return (
                      <AntDesign
                        name={isOpened ? "down" : "right"}
                        color={"#444"}
                        size={18}
                      />
                    );
                  }}
                  renderCustomizedRowChild={(item, index) => {
                    return (
                      <View
                        style={{

                          flexDirection: "row",
                          justifyContent: "space-around",
                        }}
                      >
                        <Text>{item}</Text>
                      </View>
                    );
                  }}
                />
              }
              {homework &&
                <View style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 15 }}>
                  <Text>Ngày nộp bài</Text>
                  <View style={{ flex: 1 }} />
                  <Pressable onPress={showMode} style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <TextInput
                      style={[styles.textInput]}
                      editable={false}
                      placeholder={
                        textDate
                          ? textDate
                          : "Chọn ngày nộp bài"
                      }
                      placeholderTextColor={
                        textDate ? "black" : "#AAAAAA"
                      }
                    ></TextInput>
                    <AntDesign
                      name={"right"}
                      color={"#444"}
                      size={18}
                      style={{ marginLeft: 10 }}
                    />
                  </Pressable>
                </View>
              }



              {showDate && (
                <DateTimePicker
                  display="spinner"
                  mode="date"
                  dateFormat="shortdate"
                  value={expireDate}
                  maximumDate={new Date(Date.now() + 7 * 24 * 60 * 60 * 1000)}
                  minimumDate={currentDay}
                  onChange={onchangeDate}
                  textColor="red"
                />
              )}
              {file &&
                <View style={{ alignItems: 'center' }}>
                  <View >
                    <TouchableOpacity style={{ position: 'absolute', top: 3, zIndex: 30 }} onPress={() => {
                      SetFile()
                    }}>
                      {file.mimeType === "image/jpeg" && <Ionicons name="close" size={24} color="red" />}
                    </TouchableOpacity>
                    <Image resizeMode="cover" style={{
                      width: file?.mimeType === "image/jpeg" ? ImageWidth * maxWidth : 0,
                      height: file?.mimeType === "image/jpeg" ? 200 : 0,
                      aspectRatio: isNaN(size) ? 1 / 1 : size > 1 ? 1.7 / 1 : size,
                      marginTop: 5,
                      borderColor: '#312921',
                      borderWidth: 1,
                      borderRadius: 10,

                    }} source={{ uri: file?.uri }} />

                  </View>
                </View>
              }

              {file?.mimeType !== "image/jpeg" && file !== undefined &&
                <TouchableOpacity style={{ flexDirection: 'row', borderColor: '#e9eaea', borderWidth: 1 }}>
                  <View style={{ padding: 10, borderRightWidth: 1, borderColor: '#e9eaea' }}>
                    <Image style={{ width: 40, height: 40 }} source={require('../assets/images/google-drive.png')} />
                  </View>
                  <View style={{ borderColor: '#e9eaea', padding: 10, }}>
                    <Text>{file?.name}</Text>
                    <Text style={{
                      textTransform: 'uppercase'
                    }}>{file?.name.slice(file?.name.indexOf('.') + 1)}</Text>

                  </View>
                  <TouchableOpacity style={{ position: 'absolute', top: 2, right: 0 }} onPress={() => {
                    SetFile()
                  }}>
                    <Ionicons name="close" size={24} color="black" />
                  </TouchableOpacity>
                </TouchableOpacity>
              }

              <View style={{ flexDirection: "row", justifyContent: "center" }}>
                <TouchableOpacity
                  style={styles.textButtonStyle}
                  onPress={() => {
                    dispatch(ActionshowStatus(false));
                  }}
                >
                  <Text style={styles.text}>Hủy</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={uploadPost} style={styles.textButtonStyle}>
                  <Text style={styles.text}>Ok</Text>
                </TouchableOpacity>
              </View>
            </View>
          ) : (
            <ModalStatus />
          )}
          {posts.length > 0 ? posts.map((e) => {
            return (
              e.isHomeWork ?
                <CLassHomework post={e} key={e.id} navigate={navigate} />
                : <Status post={e} key={e.id} />
            )
          }) : <View style={{ marginVertical: 10 }}>
            <Image style={{ height: 200, width: '100%' }} source={{ uri: 'https://cdn.dribbble.com/users/26068/screenshots/14638411/media/5eb84509561a92d273b7715b02fea7ea.jpg?compress=1&resize=1000x750&vertical=top' }} />
            <Text style={{ textAlign: 'center', fontSize: 16, fontWeight: 'bold' }}>Chả có cái gì ở đây cả !!!</Text>
          </View>}

        </View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

const SecondRoute = ({ navigate }) => {
  const { posts, user, host } = useSelector(state => state)
  const [homework, setHomework] = useState([])

  useEffect(() => {
    let homework = posts.filter(e => e.isHomeWork)
    setHomework(homework)
  }, [posts])

  return (
    <KeyboardAwareScrollView>
      <View style={{ alignItems: "center" }}>
        <View style={{ width: "90%" }}>
          <Text
            style={{
              borderBottomWidth: 1,
              borderBottomColor: "#54aed8",
              paddingVertical: 10,
              fontSize: 24,
              color: "#54aed8",
            }}
          >
            Bài tập của bạn
          </Text>
          {homework.map((home) => (
            <TouchableOpacity
            key={home.id}
              style={{
                flexDirection: "row",
                alignItems: "center",
                paddingVertical: 10,
              }}
              onPress={() => {
                user.id === host.id ? navigate('TeacherHomework', { 'post': home }) : navigate('DetailHomeWork', { 'post': home })
              }}
            >
              <MaterialIcons
                name="content-paste"
                size={30}
                color={"white"}
                style={{
                  padding: 5,
                  borderRadius: 50,
                  backgroundColor: "#c2c2c2",
                  marginRight: 10,
                }}
              />
              <View>
                <Text style={styleGlobal.text}>{`${home.displayName} đã đăng 1 bài tập lúc ${moment(home.createdDate).fromNow()}`}</Text>
                <Text style={{ fontWeight: "300", fontSize: 12, color: "#c2c2c2" }}>
                  {`Đến hạn 23:59 ${moment(home.expireDate).format('DD/MM/YYYY')}`}
                </Text>
              </View>
            </TouchableOpacity>
          ))}
        </View>
      </View>
    </KeyboardAwareScrollView>
  );

}


const ThirdRoute = () => {
  const { host, classStudent } = useSelector(state => state)
  return (
    <KeyboardAwareScrollView>
      <View style={{ alignItems: "center" }}>
        <View style={{ width: "70%" }}>
          <Text
            style={{
              borderBottomWidth: 1,
              borderBottomColor: "#54aed8",
              paddingVertical: 10,
              fontSize: 24,
              color: "#54aed8",
            }}
          >
            Giáo Viên
          </Text>

          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              paddingVertical: 10,
            }}
          >
            <Image
              style={{
                height: 50,
                width: 50,
                borderRadius: 50,
                marginRight: 10
              }}
              source={host.avatar ? {
                uri: host.avatar
              } : require('../assets/images/avatar.png')
              }
            />
            <Text>{host.username}</Text>
          </View>

          <View
            style={{
              flexDirection: "row",
              borderBottomWidth: 1,

              borderBottomColor: "#54aed8",
              paddingVertical: 10,
              alignItems: "center",
            }}
          >
            <Text style={{ fontSize: 24, color: "#54aed8" }}>Bạn học</Text>
            <View style={{ flex: 1 }} />
            <Text>{classStudent.length - 1} cháu</Text>
          </View>

          {
            classStudent.map((student) => {
              return (
                classStudent.length > 1 ?
                  <View key={student.id} style={{
                    flexDirection: "row",
                    alignItems: "center",
                    paddingVertical: 10,
                    borderBottomWidth: 1,
                    borderBottomColor: "#dadce0",
                    display: host.id == student.id ? 'none' : "flex"
                  }}>
                    <Image
                      style={{
                        height: 50,
                        width: 50,
                        borderRadius: 50,
                        marginRight: 10,
                      }}
                      source={student.avatar ? { uri: student.avatar } : require("../assets/images/avatar.png")}
                    />
                    <Text>{student.username}</Text>
                  </View>

                  : <View style={{ marginVertical: 10 }}>
                    <Image style={{ height: 200, width: '100%' }} source={{ uri: 'https://cdn.dribbble.com/users/26068/screenshots/14638411/media/5eb84509561a92d273b7715b02fea7ea.jpg?compress=1&resize=1000x750&vertical=top' }} />
                    <Text style={{ textAlign: 'center', fontSize: 16, fontWeight: 'bold' }}>Chả có ông cháu nào ở đây cả !!!</Text>
                  </View>

              )

            })
          }
        </View>
      </View>
    </KeyboardAwareScrollView>
  )
}





const Class = ({ route, navigation }) => {
  const { user, token, host } = useSelector((state) => state);
  const [keyboardIsShown, setKeyboardIsShown] = useState(false);
  const dispatch = useDispatch()
  const { item } = route.params
  const { navigate } = navigation
  const getHost = () => {
    let request = API + URL_API.GET_HOST_CLASS + `?classId=${item.id}`
    api_get(dispatch, request, token, (res) => {
      dispatch(ActionSaveHostClass(res.data))
    })
  }

  const getAllstudent = () => {
    let request = API + URL_API.GET_STUDENTS_CLASS + `?classId=${item.id}`
    api_get(dispatch, request, token, (res) => {
      dispatch(ActionSaveClassStudent(res.data))
    })
  }

  const edit = () => {
    dispatch(OnEdit({ ...item, OnEdit: true }))
    dispatch(actionShowModal(true))
  }

  const deleteClass = () => {
    let request = API + URL_API.DELETE_CLASS + `?id=${item.id}`
    api_delete(dispatch, request, token, res => {
      let request = API + URL_API.GET_ALL_CLASS;
      api_get(dispatch, request, token, (res) => {
        dispatch(actionSaveClass(res.data))
      })
    })
  }

  useEffect(() => {
    getHost()
    getAllstudent()
    dispatch(ActionSaveDetailClass(item))
  }, [item])

  useEffect(() => {
    Keyboard.addListener("keyboardDidShow", () => {
      setKeyboardIsShown(true);
    });
    Keyboard.addListener("keyboardDidHide", () => {
      setKeyboardIsShown(false);
    });
  });
  const [index, setIndex] = React.useState(0);
  const dropdown = ["Mời", "Sửa lớp", "Xóa lớp"]
  const dropdownStudent = ['Rời lớp']
  const [routes] = React.useState([
    { key: "first", title: "Bảng tin" },
    { key: "second", title: "Bài tập" },
    { key: "third", title: "Mọi người" },
  ]);

  const [visible, setVisible] = useState(false);
  const [visible1, setVisible1] = useState(false);
  const showModal = () => {
    setVisible(true);
  };

  const hideModal = () => {
    setVisible(false);
  };
  const showModal1 = () => {
    setVisible1(true);
  };

  const hideModal1 = () => {
    setVisible1(false);
  };

  const renderScene = ({ route }) => {
    switch (route.key) {
      case 'first':
        return <FirstRoute navigate={navigate} />;
      case 'second':
        return <SecondRoute navigate={navigate} />;
      case 'third':
        return <ThirdRoute navigate={navigate} />;
      default:
        return null;
    }
  };

  const renderTabBar = (props) => {
    return (
      <TabBar
        {...props}
        indicatorStyle={{ display: "none" }}
        pressColor={"transparent"}
        contentContainerStyle={{ backgroundColor: "#64cbd8" }}
        renderLabel={({ route, focused }) => (
          <View
            style={{
              backgroundColor: focused ? "white" : "transparent",
              width: WIDTH / 3.3,
              height: 50,
              justifyContent: "center",
              alignItems: "center",
              borderTopLeftRadius: 10,
              borderTopRightRadius: 10,
            }}
          >
            <Text
              style={{
                fontSize: 16,
                color: focused ? "#54aed8" : "white",
                fontWeight: "400",
              }}
            >
              {route.title}
            </Text>
          </View>
        )}
        style={{
          backgroundColor: "transparent",
          borderWidth: 0,
          elevation: 0,
        }}
        render
        tabStyle={{
          height: 50,
        }}
        gap={0}
      />
    );
  };
  return (
    <SafeAreaView style={[styleGlobal.container, { backgroundColor: "white" }]}>
      <Header />

      <View
        style={{
          flex: 1,
          paddingHorizontal: 20,
          backgroundColor: "#64cbd8",
          display: keyboardIsShown ? "none" : "flex",
          flexDirection: 'row',
          maxHeight: 190,
          minHeight: 100
        }}
      >
        <View
          style={{
            marginTop: "13%",
          }}
        >
          <View style={{ flexDirection: "row" }}>
            <Text
              style={[
                styleGlobal.text,
                { fontSize: 32, fontWeight: "bold", color: "white" },
              ]}
            >
              {item.className}
            </Text>

          </View>
          <View style={{ flexDirection: "row", marginTop: 10 }}>
            <View>
              <Text
                style={[
                  styleGlobal.text,
                  { color: "white", marginBottom: 10 },
                ]}
              >
                {item.content}
              </Text>
              <Text style={[styleGlobal.text, { color: "white" }]}>
                {item.description}
              </Text>
            </View>

          </View>
        </View>
        <View style={{ flex: 1 }} />
        <View style={{ marginTop: 10, alignItems: 'flex-end' }}>

          <SelectDropdown
            data={user.id === host.id ? dropdown : dropdownStudent}
            onSelect={(selectedItem, index) => {
              selectedItem === 'Sửa lớp' ? edit() : selectedItem === 'Xóa lớp' ? showModal() : selectedItem === 'Rời lớp' ? "" : showModal1()
            }}

            dropdownStyle={{
              marginTop: -30,
              borderRadius: 20
            }}
            buttonStyle={{
              paddingHorizontal: 5,
              backgroundColor: 'transparent',
            }}

            renderCustomizedButtonChild={(selectedItem, index) => {
              return (
                <View
                  style={{
                    alignItems: 'flex-end'
                  }}
                >
                  <AntDesign
                    name="ellipsis1"
                    size={24}
                    color="white"
                    style={{
                      textAlign: 'center',
                      padding: 5,
                      backgroundColor: "rgba(52, 52, 52, 0.2)",
                      borderRadius: 50,
                      height: 35, width: 35
                    }}
                  />
                </View>
              );
            }}
            renderCustomizedRowChild={(item, index) => {

              return (
                <View
                >
                  <Text style={[styleGlobal.text, { textAlign: 'center' }]}>{item}</Text>
                </View>
              );
            }}
            buttonTextAfterSelection={(selectedItem, index) => {
              return selectedItem;
            }}
            rowTextForSelection={(item, index) => {
              return item;
            }}

          />


          <Text style={[styleGlobal.text, { color: 'white', marginVertical: 5 }]}>Phòng: {item.room}</Text>
          {user.id === host.id && <Text style={[styleGlobal.text, { color: 'white' }]}>Mã lớp: {item.classCode}</Text>}
          <View style={{ alignItems: "center", position: 'absolute', bottom: 0 }}>

            <Image
              style={{
                height: 50,
                width: 50,
                borderRadius: 50,
              }}
              source={host.avatar ? {
                uri: host.avatar
              } : require('../assets/images/avatar.png')
              }
            />

            <Text style={[styleGlobal.text, { color: "white" }]}>
              {host.username}
            </Text>
          </View>

        </View>

      </View>
      <View style={{ flex: 3.7 }}>
        <TabView
          navigationState={{ index, routes }}
          renderScene={renderScene}
          onIndexChange={setIndex}
          initialLayout={{ width: WIDTH, height: 0 }}
          renderTabBar={renderTabBar}
        />
      </View>
      <ModalLogout visible={visible} back={hideModal} title={'Xóa lớp?'} content={'Bạn có chắc rằng mình muốn xóa lớp không!'} cf={'Xóa lớp'} onPress={
        () => {
          deleteClass()
          hideModal()
          navigate('Classes')
        }
      } />
      <ModalJoinClass visible={visible1} back={hideModal1} title={'Mời vào lớp'} content={'Nhập email để mời học sinh!'} placeholder={'Nhập email'} id={item.id} />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  dropS: {
    width: "93%",
    borderRadius: 10,
    marginHorizontal: 15,
    marginTop: 5,
    backgroundColor: 'white',
  },
  textd: {
    fontSize: 14,
    color: "black",
  },
  textdlabel: {
    color: "#AAAAAA",
  },
  last: {
    marginBottom: 60,
  },
  textDrop: {
    fontSize: 15,
    color: "#AAAAAA",
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  richTextContainer: {
    borderWidth: 1,
    borderColor: "#ccaf9b",
    borderRadius: 10,
  },
  richTextEditorStyle: {
    fontSize: 16,
  },

  richTextToolbarStyle: {
    backgroundColor: "#c6c3b3",
    borderColor: "#c6c3b3",
    borderWidth: 1,
  },

  textButtonStyle: {
    padding: 10,
    marginHorizontal: 10,
  },
  text: {
    fontSize: 20,
    fontWeight: "500",
    color: "#54aed8",
  },
  textInput: {
    borderRadius: 10,
    paddingVertical: 10,
    paddingLeft: 20,
    color: "black",
  },
});

export default Class;
