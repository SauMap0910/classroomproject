import {
  View,
  Text,
  SafeAreaView,
  Image,
  Dimensions,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from "react-native";
import { AntDesign, FontAwesome5 } from "@expo/vector-icons";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import React, { useState } from "react";
import Header from "../components/Header";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { useTogglePasswordVisibility } from "../helper/hidePass";
import { useDispatch, useSelector } from "react-redux";
import styleGlobal from "../assets/Style";
import { validRegister } from "../helper/valid";
import { actionChangePopupNoti, getAlert } from "../redux/actionCreators";
import { api_register } from "../helper/Api";
const Register = ({ navigation }) => {
  const { navigate } = navigation;
  const dispatch = useDispatch();
  const {
    hidePass,
    hideCfpass,
    rightIconCfPass,
    rightIconPass,
    handlePassword,
    handleCfPassword,
  } = useTogglePasswordVisibility();

  const { alert } = useSelector((state) => state);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [cfpassword, setCfpassword] = useState("");
  const userData = { username, password, cfpassword };

  const resetState = () => {
    setUsername("");
    setPassword("");
    setCfpassword("");
  };

  const validateRegister = () => {
    const check = validRegister(userData);
    if (check.errLength > 0) {
      dispatch(getAlert(check.errMsg));
      setTimeout(() => {
        dispatch(getAlert({}));
      }, 2000);
    } else {
      register(userData);
    }
  };

  const register = (data) => {
    api_register(dispatch, data, (res) => {
      resetState();
      dispatch(actionChangePopupNoti("Đăng ký thành công, chào mừng bạn!"));

      setTimeout(() => {
        dispatch(actionChangePopupNoti(""));
        navigate("Login");
      }, 1000);
    });
  };

  return (
    <SafeAreaView style={[styleGlobal.container, { backgroundColor: "white" }]}>
      <KeyboardAwareScrollView>
        <View style={{ marginHorizontal: 20 }}>
          <Header />
          <Image
            source={require("../assets/images/anh4.webp")}
            style={{
              height: 320,
              width: Dimensions.get("window").width - 40,
            }}
          />
          <Text style={styleGlobal.textTitle}>Đăng ký</Text>
          <View style={styles.searchSection}>
            <AntDesign
              name="user"
              size={24}
              color="black"
              style={styles.searchIcon}
            />
            <TextInput
              style={[styles.input]}
              placeholder="Tên đăng nhập"
              value={username}
              onChangeText={(value) => {
                setUsername(value);
              }}
            />
          </View>
          <Text style={styles.err}>{alert.username ? alert.username : ""}</Text>
          <View style={styles.searchSection}>
            <AntDesign
              name="lock"
              size={24}
              color="black"
              style={styles.searchIcon}
            />
            <TextInput
              style={[styles.input]}
              placeholder="Mật khẩu"
              secureTextEntry={hidePass}
              value={password}
              onChangeText={(value) => {
                setPassword(value);
              }}
            />

            <TouchableOpacity
              style={{
                position: "absolute",
                right: 0,
              }}
              onPress={handlePassword}
            >
              <FontAwesome5 name={rightIconPass} size={24} color="black" />
            </TouchableOpacity>
          </View>
          <Text style={styles.err}>{alert.password ? alert.password : ""}</Text>

          <View style={styles.searchSection}>
            <MaterialCommunityIcons
              name="shield-check-outline"
              size={24}
              color="black"
              style={styles.searchIcon}
            />
            <TextInput
              style={[styles.input]}
              placeholder="Xác nhận mật khẩu"
              secureTextEntry={hideCfpass}
              value={cfpassword}
              onChangeText={(value) => {
                setCfpassword(value);
              }}
            />
            <TouchableOpacity
              style={{ position: "absolute", right: 0 }}
              onPress={handleCfPassword}
            >
              <FontAwesome5 name={rightIconCfPass} size={24} color="black" />
            </TouchableOpacity>
          </View>
          <Text style={styles.err}>
            {alert.cfpassword ? alert.cfpassword : ""}
          </Text>

          <Text
            style={[
              styleGlobal.text,
              {
                color: "black",
                textAlign: "center",
                lineHeight: 24,
                marginHorizontal: 20,
              },
            ]}
          >
            Bằng việc đăng ký, bạn đã chấp nhận
            <Text style={{ color: "blue" }}> điều khoản </Text>
            và <Text style={{ color: "blue" }}>dịch vụ </Text>
            của chúng tôi
          </Text>
          <TouchableOpacity
            style={{
              backgroundColor: "black",
              padding: 15,
              borderRadius: 20,
              marginVertical: 10,
            }}
            onPress={validateRegister}
          >
            <Text
              style={{
                color: "white",
                textAlign: "center",
                fontSize: 16,
                fontWeight: "500",
              }}
            >
              Đăng ký
            </Text>
          </TouchableOpacity>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
            }}
          >
            <Text style={{ fontSize: 16 }}>Thành viên cũ? </Text>
            <TouchableOpacity
              onPress={() => {
                navigate("Login");
              }}
            >
              <Text style={{ color: "#0065ff", fontSize: 16 }}>Đăng nhập</Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  searchSection: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
  },
  searchIcon: { padding: 20 },
  input: {
    flex: 1,
    paddingLeft: 0,
    backgroundColor: "#fff",
    color: "#424242",
    borderBottomColor: "#DADEE3",
    borderBottomWidth: 1,
  },
  texta: {
    color: "#0065ff",
  },
  err: {
    marginLeft: "18%",
    color: "red",
  },
});

export default Register;
