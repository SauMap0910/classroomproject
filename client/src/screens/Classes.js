import {
  View,
  Text,
  SafeAreaView,
  FlatList,
  Image,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import React, { useState } from "react";
import * as Progress from "react-native-progress";
import ClassCard from "../components/ClassCard";
import styleGlobal from "../assets/Style";
import ModalBottom from "../components/myComponents/ModalBottom";
import Modal from "react-native-modal";
import ModalJoinClass from "../components/myComponents/ModalJoinClass";
import {
  AntDesign,
  MaterialCommunityIcons,
  FontAwesome5,
} from "@expo/vector-icons";
import { useDispatch, useSelector } from "react-redux";
import ModalCreateClass from "../components/myComponents/ModalCreateClass";
import { api_get, URL_API } from "../helper/Api";
import { API } from "../Config";
import { useEffect } from "react";
import { actionSaveClass } from "../redux/actionCreators";
const windowWidth = Dimensions.get("window").width;
const Classes = ({ navigation }) => {
  const { user, token,Class } = useSelector((state) => state);
  const { navigate } = navigation;
  const [visible, setVisible] = useState(false);
  const [visibleFancy, setVisibleFancy] = useState(false);
  const dispatch = useDispatch();

  const showModalBottom = () => {
    setVisible(true);
  };

  const hideModalBottom = () => {
    setVisible(false);
  };

  const showModalFancy = () => {
    setVisibleFancy(true);
  };

  const hideModalFancy = () => {
    setVisibleFancy(false);
  };

  useEffect(() => {
    let request = API + URL_API.GET_ALL_CLASS;
    api_get(dispatch,request,token, (res) => {
      dispatch(actionSaveClass(res.data))
    })
  }, []);

  const data = [
    {
      icon: "fire",
      text: "Tất cả",
    },
    {
      icon: "heart",
      text: "Phổ biến",
    },
    {
      icon: "star",
      text: "Mới nhất",
    },
    {
      icon: "bookmark",
      text: "Nâng cao",
    },
  ];

  return (
    <SafeAreaView
      style={[styleGlobal.container, { backgroundColor: "#e7eff5" }]}
    >
      <View
        style={{
          flexDirection: "row",
          paddingHorizontal: 20,
          paddingVertical: 10,
          backgroundColor: "white",
        }}
      >
        <View>
          <Text
            style={[styleGlobal.text, { color: "#c9c9c9", fontWeight: "600" }]}
          >
            Xin chào,
          </Text>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
            }}
          >
            <Text
              style={{
                fontSize: 32,
                color: "black",
                fontWeight: "600",
                marginRight: 30,
              }}
            >
              {user.username}
            </Text>
            <MaterialCommunityIcons
              name="hand-wave-outline"
              size={40}
              color="#eeb354"
            />
          </View>
        </View>
        <View style={{ flex: 1 }} />
        {user.avatar ? (
          <Image
            style={{
              height: 50,
              width: 50,
              borderRadius: 50,
              alignSelf: "center",
            }}
            source={{ uri: user.avatar }}
          />
        ) : (
          <Image
            style={{
              height: 50,
              width: 50,
              borderRadius: 50,
              alignSelf: "center",
            }}
            source={require("../assets/images/avatar.png")}
          />
        )}
      </View>

      <View
        style={{
          backgroundColor: "#5bbff3",
          marginVertical: 15,
          height: 120,
          justifyContent: "center",
          borderRadius: 30,
          borderTopStartRadius: 50,
          borderTopEndRadius: 50,
          marginHorizontal: 20,
        }}
      >
        <View
          style={{
            marginHorizontal: 30,
            marginBottom: 25,
          }}
        >
          <Text
            style={{
              color: "white",
              fontSize: 16,
              fontWeight: "700",
              lineHeight: 24,
            }}
          >
            Đã học hôm nay
          </Text>
          <View style={{ flexDirection: "row" }}>
            <Text style={{ fontSize: 24, color: "white", fontWeight: "700" }}>
              46min
            </Text>
            <Text
              style={{
                fontSize: 14,
                padding: 10,
                color: "white",
                fontWeight: "200",
              }}
            >
              /60min
            </Text>
          </View>
          <Progress.Bar
            width={150}
            progress={0.8}
            height={10}
            borderRadius={10}
            color={"white"}
          />
        </View>
        <TouchableOpacity
          style={{
            position: "absolute",
            bottom: -20,
            left: "10%",
            backgroundColor: "white",
            padding: 5,
            borderRadius: 50,
          }}
          onPress={showModalBottom}
        >
          <Text>
            <AntDesign name="plus" size={24} color="#4e4e5c" />
          </Text>
        </TouchableOpacity>
      </View>
      <Image
        source={require("../assets/images/graduation.png")}
        style={{
          height: 150,
          width: 130,
          position: "absolute",
          right: 0,
          top: "15%",
        }}
      />

      <Text
        style={{
          marginHorizontal: 20,
          marginVertical: 15,
          fontSize: 24,
          fontWeight: "500",
          color: "black",
        }}
      >
        Lớp học
      </Text>
      <View
        style={{
          marginHorizontal: 20,
          justifyContent: "space-around",
        }}
      >
        <FlatList
          data={data}
          contentContainerStyle={{
            alignItems: "center",
          }}
          renderItem={({ item, index }) => (
            <TouchableOpacity
              key={item.text}
              style={{
                flexDirection: "row",
                backgroundColor: "white",
                alignItems: "center",
                padding: 8,
                borderRadius: 25,
                width: windowWidth / 2.4,
                margin: 5,
              }}
            >
              <FontAwesome5
                name={item.icon}
                size={16}
                color="white"
                style={{
                  backgroundColor:
                    index === 0
                      ? "#50b7f5"
                      : index == 2
                      ? "#767ca4"
                      : index == 1
                      ? "#eba927"
                      : "#53a36f",
                  borderRadius: 50,
                  padding: 8,
                  textAlign: "center",
                }}
              />
              <Text
                style={{
                  color: "black",
                  marginHorizontal: 10,
                  fontWeight: "600",
                }}
              >
                {item.text}
              </Text>
            </TouchableOpacity>
          )}
          keyExtractor={(item) => item.text}
          numColumns={2}
        />
      </View>
      {Class.length > 0 ? (
        <FlatList
          data={Class}
          style={{ marginHorizontal: 20, marginTop: 15 }}
          horizontal
          keyExtractor={(item, index) => {
            return item.classCode;
          }}
          renderItem={({ item, index }) => (
            <ClassCard
            key={item.classCode}
              item={item}
              index={index}
              navigate={navigate}
              length={Class.length - 1}
            />
          )}
        />
      ) : (
        <View style={{ alignItems: "center", marginVertical: 20 }}>
          <Image
            source={{uri:'https://cdn.dribbble.com/users/1507491/screenshots/4945826/media/116a8ebc414c519ad1cfc0fe63f79d3e.jpg?compress=1&resize=800x600&vertical=top'}}
            style={{ height: 200, width: "80%" }}
          />
          <Text
            style={[
              styleGlobal.text,
              {
                textAlign: "center",
                marginVertical: 10,
                marginHorizontal:'10%',
                fontWeight:'bold'
              },
            ]}
          >
            Bạn chưa tham gia hay mở lớp học nào. Tham gia ngay!!!
          </Text>
        </View>
      )}

      <Modal
        isVisible={visible}
        swipeDirection={["up", "left", "right", "down"]}
        useNativeDriver={true}
        style={{
          justifyContent: "flex-end",
          margin: 0,
        }}
      >
        <ModalBottom
          onPress={hideModalBottom}
          showModalFancy={showModalFancy}
        />
      </Modal>

  
        <ModalJoinClass title={'Tham gia lớp học'} content={'Đề nghị giáo viên của bạn cung cấp mã lớp rồi nhập mã đó vào đây'} back={hideModalFancy} visible={visibleFancy} placeholder={'Mã lớp'} />
     
      <ModalCreateClass navigate={navigate}/>
    </SafeAreaView>
  );
};

export default Classes;
