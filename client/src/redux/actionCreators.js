import * as types from "./types";
import { removeMulti, setItem } from "../helper/AsyncStorage";

export function actionShowLoading(isShowLoading) {
  return {
    type: types.SHOW_LOADING,
    isShowLoading,
  };
}

export function actionShowModal(show) {
  return {
    type: types.SHOW_MODAL,
    show,
  };
}

export const getAlert = (alert) => {
  return ({
    type: types.ALERT,
    alert,
  });
};

export function actionSaveToken(token) {
  setItem("token", token, "TEXT");
  return {
    type: types.SAVE_TOKEN,
    token,
  };
}

export function actionSaveClass(classes) {
  return {
    type: types.GET_CLASSES,
    classes,
  };
}

export function actionSaveUser(user) {
  setItem("user", user, "JSON");
  return {
    type: types.SAVE_USER_INFO,
    user,
  };
}

export function actionChangePopupNoti(showPopupNoti) {
  return {
    type: types.CHANGE_POPUP_NOTI,
    showPopupNoti,
  };
}

export function actionChangePopupConfirm(showPopupConfirm) {
  return {
    type: types.CHANGE_POPUP_CONFIRM,
    showPopupConfirm,
  };
}

export function actionShowPopup(cmd, params = null) {
  return {
    type: types.SHOW_POPUP,
    popup: { cmd, params },
  };
}

export function logout(keys) {
  removeMulti(keys);
  return {
    type: types.USER_LOGOUT,
    token: "",
  };
}

export function ActionshowStatus(status) {
  return {
    type: types.SHOW_STATUS,
    status,
  };
}

export function ActionshowEditUser(editUser) {
  return {
    type: types.EDIT_USER,
    editUser,
  };
}



export function ActionSaveHostClass(host) {
  return {
    type: types.SAVE_HOST,
    host,
  };
}

export function ActionSaveClassStudent(ClassStudent) {
  return {
    type: types.SAVE_CLASS_STUDENT,
    ClassStudent,
  };
}

export function OnEdit(onEdit) {
  return {
    type: types.ON_EDIT,
    onEdit,
  };
}

export function ActionInvite(invite) {
  return {
    type: types.INVITE,
    invite,
  };
}

export function ActionSaveDetailClass(Dclass) {
  return {
    type: types.SAVE_CLASS,
    Dclass,
  };
}

export function ActionSavePost(statePost) {
  return {
    type: types.SAVE_POST,
    statePost,
  };
}

export function ActionSaveHomework(homeworks) {
  return {
    type: types.SAVE_HOMEWORK,
    homeworks,
  };
}