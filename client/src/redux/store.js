import { createStore, applyMiddleware, combineReducers } from "redux";
import thunk from "redux-thunk";
import * as types from "./types";

let alert = {};
const Alert = (state = alert, action) => {
  if (action.type === types.ALERT) {
    alert = action.alert;
    return alert;
  }
  return state;
};

let _token = "";
const token = (state = _token, action) => {
  switch (action.type) {
    case types.SAVE_TOKEN:
      _token = action.token;
      return _token;
    case types.USER_LOGOUT:
      _token = action.token;
      return _token;
    default:
      return state;
  }
};

let userData = {};
const user = (state = userData, action) => {
  if (action.type === types.SAVE_USER_INFO) {
    userData = action.user;
    return userData;
  }
  return state;
};

let isShowLoading = false;
const showLoading = (state = isShowLoading, action) => {
  if (action.type === types.SHOW_LOADING) {
    isShowLoading = action.isShowLoading;
    return isShowLoading;
  }
  return state;
};

let showPopupNoti = "";
const changePopupNoti = (state = showPopupNoti, action) => {
  if (action.type === types.CHANGE_POPUP_NOTI) {
    showPopupNoti = action.showPopupNoti;
    return showPopupNoti;
  }
  return state;
};

let showPopupConfirm = {
  title: null,
  content: null,
  action: null,
  cancel: "",
  confirm: "",
};
const changePopupConfirm = (state = showPopupConfirm, action) => {
  if (action.type === types.CHANGE_POPUP_CONFIRM) {
    showPopupConfirm = action.showPopupConfirm;
    return showPopupConfirm;
  }
  return state;
};

let show = false;
const showModal = (state = show, action) => {
  if (action.type === types.SHOW_MODAL) {
    show = action.show;
    return show;
  }
  return state;
};

let status = false;
const showStatus = (state = status, action) => {
  if (action.type === types.SHOW_STATUS) {
    status = action.status;
    return status;
  }
  return state;
};

let editUser = false;
const editUserP = (state = editUser, action) => {
  if (action.type === types.EDIT_USER) {
    editUser = action.editUser;
    return editUser;
  }
  return state;
};

let classes = [];
const Class = (state = classes, action) => {
  if (action.type === types.GET_CLASSES) {
    classes = action.classes;
    return classes;
  }
  return state;
};

let Host = {}
const host = (state = Host, action) => {
  if (action.type === types.SAVE_HOST) {
    Host = action.host;
    return Host;
  }
  return state;
};

let ClassStudent = []
const classStudent = (state = ClassStudent, action) => {
  if(action.type === types.SAVE_CLASS_STUDENT){
    ClassStudent = action.ClassStudent
    return ClassStudent
  }
  return state
}

let onEdit = []
const SetOnEdit = (state = onEdit, action) => {
  if(action.type === types.ON_EDIT) {
    onEdit = action.onEdit
    return onEdit
  }
  return state
}

let invite = false
const setInvite = (state = invite, action) => {
  if(action.type === types.INVITE) {
    invite = action.invite
    return invite
  }
  return state
}

let classItem = {}
const detailClass = (state = classItem, action) => {
  if(action.type === types.SAVE_CLASS) {
    classItem = action.Dclass
    return classItem
  }
  return state
}

let post = []
const posts = (state = post,action) => {
  if(action.type === types.SAVE_POST) {
    post = action.statePost
    return post
  }
  return state
}

let homework = []
const homeworks = (state= homework,action) => {
  if(action.type === types.SAVE_HOMEWORK) {
    homework = action.homeworks
    return homework
  }
  return state
}


const reducer = combineReducers({
  alert: Alert,
  token: token,
  user: user,
  showLoading: showLoading,
  showModal: showModal,
  changePopupNoti: changePopupNoti,
  changePopupConfirm: changePopupConfirm,
  showStatus: showStatus,
  editUserP: editUserP,
  Class: Class,
  host: host,
  classStudent: classStudent,
  onEdit: SetOnEdit,
  invite: setInvite,
  detailClass: detailClass,
  posts: posts,
  homeworks: homeworks
});

const store = createStore(reducer, applyMiddleware(thunk));
export default store;
