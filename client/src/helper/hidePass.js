import { useState } from "react";
export const useTogglePasswordVisibility = () => {
  const [hidePass, setHidePass] = useState(true);
  const [hideCfpass, setHideCfpass] = useState(true);
  const [rightIconPass, setRightIconPass] = useState("eye");
  const [rightIconCfPass, setRightIconCfPass] = useState("eye");

  const handlePassword = () => {
    if (rightIconPass === "eye") {
      setRightIconPass("eye-slash");
      setHidePass(!hidePass);
    } else if (rightIconPass === "eye-slash") {
      setRightIconPass("eye");
      setHidePass(!hidePass);
    }
  };

  const handleCfPassword = () => {
    if (rightIconCfPass === "eye") {
      setRightIconCfPass("eye-slash");
      setHideCfpass(!hideCfpass);
    } else if (rightIconCfPass === "eye-slash") {
      setRightIconCfPass("eye");
      setHideCfpass(!hideCfpass);
    }
  };

  return {
    hidePass,
    hideCfpass,
    rightIconPass,
    rightIconCfPass,
    handleCfPassword,
    handlePassword,
  };
};
