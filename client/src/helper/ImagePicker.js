import { launchCamera, launchImageLibrary } from "react-native-image-picker";
import Config from "../Config";
import { request, PERMISSIONS } from "react-native-permissions";

// More info on all the options is below in the README...just some common use cases shown here
let default_options = {
  title: "Chọn Ảnh",
  cancelButtonTitle: "Huỷ",
  takePhotoButtonTitle: "Chụp ảnh",
  chooseFromLibraryButtonTitle: "Chọn từ thư viện",
  mediaType: "photo",
  quality: 0.5,
  // storageOptions: {
  //     skipBackup: true,
  //     path: 'images'
  // }
};

export const pick = (callback, options = default_options) => {
  // request(PERMISSIONS.IOS.PHOTO_LIBRARY).then((result) => {
  //     console.log('result permission', result)
  // });
  launchImageLibrary(options).then((response) => {
    if (response.didCancel) {
      console.log("User cancelled image picker");
    } else if (response.error) {
      console.log("ImagePicker Error: ", response.error);
    } else if (response.customButton) {
      console.log("User tapped custom button: ", response.customButton);
    } else {
      console.log("response", response);
      let source = { uri: response.uri };
      // You can also display the image using data:
      // let source = { uri: 'data:image/jpeg;base64,' + response.data };
      let base64 = "data:image/jpeg;base64," + response.assets[0].base64;
      callback(source, base64);
    }
  });

  // launchImageLibrary(options, (response) => {
  //     if (response.didCancel) {
  //         console.log('User cancelled image picker');
  //     }
  //     else if (response.error) {
  //         console.log('ImagePicker Error: ', response.error);
  //     }
  //     else if (response.customButton) {
  //         console.log('User tapped custom button: ', response.customButton);
  //     }
  //     else {
  //         console.log('response', response);
  //         let source = { uri: response.uri };
  //         // You can also display the image using data:
  //         // let source = { uri: 'data:image/jpeg;base64,' + response.data };
  //         let base64 = 'data:image/jpeg;base64,' + response.assets[0].base64;
  //         callback(source, base64);
  //     }
  // });
};

export const openCamera = (props, callback, options = default_options) => {
  launchCamera(options, (response) => {
    if (response.didCancel) {
      console.log("User cancelled image picker");
    } else if (response.error) {
      console.log("ImagePicker Error: ", response.error);
    } else if (response.errorCode) {
      console.log("ImagePicker Error: ", response.errorCode);
      if (response.errorCode === "camera_unavailable") {
        props.actionChangePopupNoti("Camera không khả dụng");
        // alert('Camera không khả dụng')
      }
    } else if (response.customButton) {
      console.log("User tapped custom button: ", response.customButton);
    } else {
      console.log("response image picker", response);
      let source = { uri: response.uri };
      // You can also display the image using data:
      // let source = { uri: 'data:image/jpeg;base64,' + response.data };
      let base64 = "data:image/jpeg;base64," + response.assets[0].base64;
      callback(source, base64);
    }
  });
};
