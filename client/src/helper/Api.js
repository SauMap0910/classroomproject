import axios from "axios";
import store from "../redux/store";
import {
  actionChangePopupNoti,
  actionSaveUser,
  ActionshowEditUser,
  actionShowLoading,
  getAlert,
} from "../redux/actionCreators";
import { API } from "../Config";

export const URL_API = {
  LOGIN: "User/login",
  REGISTER: "User/create",
  UPDATE_INFO: "User/update-info",
  GET_USER: "User/get-student-by-id/",
  CREATE_CLASS: "Classroom/create-class",
  UPDATE_CLASS: "Classroom/update-class",
  DELETE_CLASS: "Classroom/delete-class",
  GET_ALL_CLASS: "Classroom/get-all-class",
  JOIN_CLASS: "Classroom/join-class-by-classcode",
  GET_HOST_CLASS: "Classroom/get-host-class",
  GET_STUDENTS_CLASS: 'Classroom/get-all-student',
  ADD_STUDENTS : 'Classroom/add-user-to-class',
  CREATE_POST: 'Post/create-post',
  GET_POST: 'Post/get-all',
  UPDATE_POST: 'Post/update-post',
  DELETE_POST: 'Post/delete-post',
  GET_HOMEWORK: 'Post/get-all-homework-by-postId',
  SUBMIT_HOMEWORK:'Post/submit-homework',
  SUBMIT_POINT:'Post/mark-point'
};

export const api_login = (dispatch, data, callback, callback_error) => {
  if (dispatch) dispatch(actionShowLoading(true));
  let token = data.account_token || store.getState().token || "";
  let config = {
    headers: { "Content-Type": "application/json" },
  };
  if (token) {
    config = {
      headers: { "Content-Type": "application/json", Authorization: token },
    };
  }
  axios
    .post(API + URL_API.LOGIN, data, config)
    .then((response) => {
      if (dispatch) dispatch(actionShowLoading(false));
      if (response.status === 200) {
        callback(response.data);
      }
    })
    .catch((response_error) => {
      if (dispatch) dispatch(actionShowLoading(false));
      console.log(response_error);

      if (response_error.response && response_error.response.data) {
        dispatch(actionChangePopupNoti(response_error.response.data.message));
      } else if (callback_error) {
        callback_error();
        return;
      } else {
        console.log(`error [POST] : ${response_error}`);
      }
    });
};

export const api_register = (dispatch, data, callback, callback_error) => {
  if (dispatch) dispatch(actionShowLoading(true));
  let config = {
    headers: { "Content-Type": "application/json" },
  };

  axios
    .post(API + URL_API.REGISTER, data, config)
    .then((response) => {
      if (dispatch) dispatch(actionShowLoading(false));
      if (response.status === 200) {
        callback(response.data);
      }
    })
    .catch((response_error) => {
      if (dispatch) dispatch(actionShowLoading(false));
      console.log(response_error);
      if (dispatch) dispatch(actionShowLoading(false));
      console.log(response_error);

      if (response_error.response && response_error.response.data) {
        dispatch(actionChangePopupNoti(response_error.response.data.message));
      } else if (callback_error) {
        callback_error();
        return;
      } else {
        console.log(`error [POST] : ${response_error}`);
      }
    });
};

export const api_post = (dispatch, request, data, callback, callback_error) => {
  if (dispatch) dispatch(actionShowLoading(true));
  console.log('data',data)
  let token = data.account_token || store.getState().token || "";

  let config = {
    headers: { "Content-Type": "application/json" },
  };
  if (token) {
    config = {
      headers: { "Content-Type": "application/json", Authorization: token },
    };
  }
  axios
    .post(request, data, config)
    .then((response) => {
      if (dispatch) dispatch(actionShowLoading(false));
      console.log("post", response);
      dispatch(actionChangePopupNoti("Tạo thành công"));
      if (response.status === 200) {
        callback(response.data);
      }
    })
    .catch((response_error) => {
      if (dispatch) dispatch(actionShowLoading(false));
      console.log(response_error);

      if (response_error.response && response_error.response.data) {
        dispatch(actionChangePopupNoti(response_error.response.data.message));
      } else if (callback_error) {
        callback_error();
        return;
      } else {
        console.log(`error [POST] : ${response_error}`);
      }
    });
};

export const api_post1 = (dispatch, request, data, callback, callback_error) => {
  if (dispatch) dispatch(actionShowLoading(true));
  let token = data.account_token || store.getState().token || "";

  let config = {
    headers: { "Content-Type": "application/json" },
  };
  if (token) {
    config = {
      headers: { "Content-Type": "multipart/form-data", Authorization: token },
    };
  }
  axios
    .post(request, data, config)
    .then((response) => {
      if (dispatch) dispatch(actionShowLoading(false));
      console.log("post", response);
      dispatch(actionChangePopupNoti("Tạo thành công"));
      if (response.status === 200) {
        callback(response.data);
      }
    })
    .catch((response_error) => {
      if (dispatch) dispatch(actionShowLoading(false));
      console.log(response_error);

      if (response_error.response && response_error.response.data) {
        dispatch(actionChangePopupNoti(response_error.response.data.message));
      } else if (callback_error) {
        callback_error();
        return;
      } else {
        console.log(`error [POST] : ${response_error}`);
      }
    });
};

export const api_put = async (dispatch,request,data,callback) => {
  try {
    dispatch(actionShowLoading(true))
    let token = data.account_token  || "";

    let config = {
      headers: { "Content-Type": "application/json" },
    };
    if (token) {
      config = {
        headers: { "Content-Type": "application/json", Authorization: token },
      };
    }
    const res = await axios.put(request,data,config)
    console.log('api_put',res)
    dispatch(actionChangePopupNoti('Cập nhật thành công'))
    dispatch(actionShowLoading(false))
    if(callback) callback(res)
  } catch (error) {
    dispatch(actionShowLoading(false))
    dispatch(getAlert({err: error.response.data.message}))


  }
}

// export const api_put = (dispatch, request, data, callback, callback_error) => {
//   if (dispatch) dispatch(actionShowLoading(true));
//   let token = data.account_token || store.getState().token || "";

//   let config = {
//     headers: { "Content-Type": "application/json" },
//   };
//   if (token) {
//     config = {
//       headers: { "Content-Type": "application/json", Authorization: token },
//     };
//   }
//   axios
//     .put(request, data, config)
//     .then((response) => {
//       if (dispatch) dispatch(actionShowLoading(false));
//       console.log(response);
//       dispatch(actionChangePopupNoti("Cập nhật thành công"));
//       if (response.status === 200) {
//         callback(response.data);
//       }
//     })
//     .catch((response_error) => {
//       if (dispatch) dispatch(actionShowLoading(false));
//       console.log(response_error);

//       if (response_error.response && response_error.response.data) {
//         dispatch(actionChangePopupNoti(response_error.response.data.message));
//       } else if (callback_error) {
//         callback_error();
//         return;
//       } else {
//         console.log(`error [PUT] : ${response_error}`);
//       }
//     });
// };


export const api_get = async (dispatch,request ,token, callback) => {
  try {
    dispatch(actionShowLoading(true))
    const res = await axios.get(request  ,{
      headers: { Authorization: token },
    });
    console.log('api_get:',res)
    if(callback) callback(res)

    dispatch(actionShowLoading(false))
  } catch (error) {
    dispatch(actionShowLoading(false))
    dispatch(getAlert({err: error.response.data.message}))

  }
};

export const api_delete = async (dispatch,request ,token, callback) => {
  try {
    dispatch(actionShowLoading(true))
    const res = await axios.delete(request  ,{
      headers: { Authorization: token },
    });
    console.log('api_delete:',res)
    if(callback) callback(res)

    dispatch(actionShowLoading(false))
  } catch (error) {
    dispatch(actionShowLoading(false))
    dispatch(getAlert({err: error.response.data.message}))
  }
}


export const getProfile = (dispatch, id,token) => {
  let request = API + URL_API.GET_USER + id + `?id=${id}`;
  api_get(dispatch,request,token, (res) => {
    if(res.status === 200) {
      dispatch(actionSaveUser(res.data))
      dispatch(ActionshowEditUser(false))
    }
  })
};

export const api_updateProfile = (
  dispatch,
  id,
  data,
) => {
  try {
    let request = API + URL_API.UPDATE_INFO;
    const da = { ...data, id };
    api_put(dispatch,request,da,(res) => {
      getProfile(dispatch,id,data.account_token)
    })

  } catch (error) {
  
    console.log(error)
  }
 
};

export const joinClassByClassCode = async (dispatch,data,callback) => {
  try {
    let request = API + URL_API.JOIN_CLASS + `?classCode=${data.classCode}`
    dispatch(actionShowLoading(true))
    let token = data.account_token  || "";

    let config = {
      headers: { "Content-Type": "application/json" },
    };
    if (token) {
      config = {
        headers: { "Content-Type": "application/json", Authorization: token },
      };
    }
    const res = await axios.put(request,data,config)

   if(callback) callback(res)

  } catch (error) {
    console.log(error)
  }
}

export const inviteStudent = async (dispatch,data,callback) => {
  try {
    let request = API + URL_API.ADD_STUDENTS + `?email=${data.email}&classId=${data.classId}`
    dispatch(actionShowLoading(true))
    let token = data.account_token  || "";

    let config = {
      headers: { "Content-Type": "application/json" },
    };
    if (token) {
      config = {
        headers: { "Content-Type": "application/json", Authorization: token },
      };
    }
    const res = await axios.post(request,data,config)

   if(callback) callback(res)

  } catch (error) {
    console.log(error)
  }
}
