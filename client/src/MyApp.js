import React, { useEffect } from "react";
import { View } from "react-native";
import UiTab from "./components/navigation/UiTab";
import Login from "./screens/Login";
import Register from "./screens/Register";
import Welcome from "./screens/Welcome";
import Loading from "./components/Loading";
import Popup from "./components/popup/Popup";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { NavigationContainer } from "@react-navigation/native";
import { useSelector } from "react-redux";
import DetailHomeWork from "./screens/DetailHomeWork";
import TeacherHomework from "./screens/TeacherHomework";

const Stack = createNativeStackNavigator();

const MyApp = () => {
  const { token } = useSelector((state) => state);
  return (
    <View style={{ flex: 1 }}>
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}
          initialRouteName="Welcome"
        >
          <Stack.Screen name={"Welcome"} component={Welcome} />
          <Stack.Screen name={"DetailHomeWork"} component={DetailHomeWork} />
          <Stack.Screen name={"TeacherHomework"} component={TeacherHomework} />
          {token === "" && <Stack.Screen name={"Login"} component={Login} />}

          {token === "" && (
            <Stack.Screen name={"Register"} component={Register} />
          )}

          <Stack.Screen name={"UiTab"} component={UiTab} />
        </Stack.Navigator>
      </NavigationContainer>
      <Loading />
      <Popup />
    </View>
  );
};

export default MyApp;
