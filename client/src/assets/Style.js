import { StyleSheet, StatusBar } from "react-native";

const styleGlobal = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight,
  },
  text: {
    fontSize: 16,
    color: "#4e4e5c",
    backgroundColor: "transparent",
  },
  textTitle: {
    fontSize: 32,
    fontWeight: "700",
    color: "#4e4e5c",
  },
  textBold: {
    fontSize: 16,
    color: "#242424",
    fontWeight: "700",
  },
  textThin: {
    fontSize: 16,
    color: "#6F6F70",
  },

  shadow: {
    shadowColor: "#aaa",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.3,
    shadowRadius: 5,
    elevation: 2,
  },
  buttonText: {
    backgroundColor: "#18A47B",
    color: "#fff",
    fontSize: 12,
    textAlign: "center",
    borderRadius: 24,
    overflow: "hidden",
    paddingHorizontal: 25,
    paddingVertical: 12,
  },
});

export default styleGlobal;
