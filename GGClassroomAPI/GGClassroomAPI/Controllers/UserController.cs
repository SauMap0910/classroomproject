﻿using GGClassroomAPI.Dtos.Users;
using GGClassroomAPI.Entities;
using GGClassroomAPI.Exceptions;
using GGClassroomAPI.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace GGClassroomAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ApiControllerBase
    {
        private readonly IUserService _userService;

        public UserController(
            IUserService userService,
            ILogger<UserController> logger) : base(logger)
        {
            _userService = userService;
        }

        [HttpPost("create")]
        public IActionResult Create(CreateUserDto input)
        {
            try
            {
                _userService.Create(input);
                return Ok();
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        [HttpPost("login")]
        public IActionResult Login(LoginDto input)
        {
            try
            {
                LoginResultDto result = _userService.Login(input);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        [HttpPut("update-info")]
        public IActionResult Update(UpdateUserDto input)
        {
            try
            {
                _userService.UpdateInfo(input);
                return Ok();
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        [HttpGet("get-student-by-id/{id}")]
        public IActionResult GetById([FromQuery] int id)
        {
            try
            {
                User user = _userService.GetbyId(id);
                return Ok(user);
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }
    }
}
