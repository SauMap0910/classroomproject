﻿using GGClassroomAPI.Dtos.Classrooms;
using GGClassroomAPI.Migrations;
using GGClassroomAPI.Services.Interfaces;
using GGClassroomAPI.Utils;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Net.Http.Headers;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Security.Claims;
using System.Text;

namespace GGClassroomAPI.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class ClassroomController : ApiControllerBase
    {
        private readonly IClassroomService _classroomService;

        public ClassroomController(
            IClassroomService classroomService,
            ILogger<ClassroomController> logger) : base(logger)
        {
            _classroomService = classroomService;
        }

        /// <summary>
        /// Thêm mới lớp học
        /// </summary>
        /// <param name="authorization"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("create-class")]
        public IActionResult CreateClass([FromHeader] string authorization, CreateClassroomDto input)
        {
            try
            {
                int userId = CommonUtils.GetUserIdByToken(authorization);
                _classroomService.CreateClass(input, userId);
                return Ok();
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        /// <summary>
        /// Cập nhật lớp học
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut("update-class")]
        public IActionResult UpdateClass(UpdateClassroomDto input)
        {
            try
            {
                _classroomService.UpdateClass(input);
                return Ok();
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        /// <summary>
        /// Xóa lớp học
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("delete-class")]
        public IActionResult DeleteClass(int id)
        {
            try
            {
                _classroomService.DeleteClass(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        /// <summary>
        /// Lấy danh sách lớp học
        /// </summary>
        /// <param name="authorization"></param>
        /// <returns></returns>
        [HttpGet("get-all-class")]
        public IActionResult GetAllClassroom([FromHeader] string authorization)
        {
            try
            {
                int userId = CommonUtils.GetUserIdByToken(authorization);
                var result = _classroomService.GetAllClassroom(userId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        /// <summary>
        /// Thêm người dùng vào lớp học qua email
        /// </summary>
        /// <param name="email"></param>
        /// <param name="classId"></param>
        /// <returns></returns>
        [HttpPost("add-user-to-class")]
        public IActionResult AddUserToClassByEmail(string email, int classId)
        {
            try
            {
                _classroomService.AddUserToClassByEmail(email, classId);
                return Ok();
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        /// <summary>
        /// Tham gia lớp học qua classCode
        /// </summary>
        /// <param name="authorization"></param>
        /// <param name="classCode"></param>
        /// <returns></returns>
        [HttpPut("join-class-by-classcode")]
        public IActionResult JoinClassByClassCode([FromHeader] string authorization, string classCode)
        {
            try
            {
                int userId = CommonUtils.GetUserIdByToken(authorization);
                _classroomService.JoinClassByClassCode(userId, classCode);
                return Ok();
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        [HttpGet("get-all-student")]
        public IActionResult GetAllStudentInClass(int classId)
        {
            try
            {
                var result = _classroomService.GetAllStudentInClass(classId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        [HttpGet("get-host-class")]
        public IActionResult GetHostOfClass(int classId)
        {
            try
            {
                var result = _classroomService.GetHostOfClass(classId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }
    }
}
