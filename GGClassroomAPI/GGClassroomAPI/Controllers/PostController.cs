﻿using GGClassroomAPI.Dtos.Classrooms;
using GGClassroomAPI.Dtos.Post;
using GGClassroomAPI.Migrations;
using GGClassroomAPI.Services.Implements;
using GGClassroomAPI.Services.Interfaces;
using GGClassroomAPI.Utils;
using Microsoft.AspNetCore.Mvc;

namespace GGClassroomAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ApiControllerBase
    {
        private readonly IPostService _postService;
        private readonly IConfiguration _configuration;

        public PostController(
            IPostService postService,
            ILogger<ClassroomController> logger,
            IConfiguration config) : base(logger)
        {
            _postService = postService;
            _configuration = config;
        }

        [HttpPost("upload")]
        public async Task<IActionResult> OnPostUploadAsync(List<IFormFile> files)
        {
            try
            {
                long size = files.Sum(f => f.Length);
                string uploads = Path.Combine(_configuration["HomeWorkUrl"], "Uploads");
                Directory.CreateDirectory(uploads); //Create directory if it doesn't exist 
                foreach (IFormFile file in files)
                {
                    if (file.Length > 0)
                    {
                        string filePath = Path.Combine(uploads, file.FileName);
                        using (Stream fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write))
                        {
                            file.CopyTo(fileStream);
                        }
                    }
                }
                return Ok(new { count = files.Count, size });
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        /// <summary>
        /// Thêm mới post
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("create-post")]
        public IActionResult CreatePost([FromHeader] string authorization, [FromForm] CreatePostDto input)
        {
            try
            {
                int userId = CommonUtils.GetUserIdByToken(authorization);
                _postService.CreatePost(input, userId);
                return Ok();
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        /// <summary>
        /// Cập nhật post
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut("update-post")]
        public IActionResult UpdatePost(UpdatePostDto input)
        {
            try
            {
                _postService.UpdatePost(input);
                return Ok();
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        /// <summary>
        /// Xóa post
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("delete-post")]
        public IActionResult DeletePost(int id)
        {
            try
            {
                _postService.DeletePost(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        /// <summary>
        /// Xem danh sách post
        /// </summary>
        /// <param name="classId"></param>
        /// <returns></returns>
        [HttpGet("get-all")]
        public IActionResult GetAllPost(int classId)
        {
            try
            {
                var result = _postService.GetAllPost(classId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        /// <summary>
        /// Lấy danh sách đường dẫn file qua url
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        [HttpGet("get-files")]
        public IActionResult GetFiles(string url)
        {
            try
            {
                var result = _postService.GetFiles(url);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        /// <summary>
        /// Tải file qua đường dẫn (tới tên file)
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        [HttpGet("get-file-by-path")]
        public IActionResult GetFileByPath(string path)
        {
            try
            {
                var result = _postService.GetFileByPath(path);
                return File(result.FileContents, result.ContentType, result.FileName);
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        /// <summary>
        /// Xóa file
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        [HttpGet("delete-file-by-path")]
        public IActionResult DeleteFileByPath(string path)
        {
            try
            {
                _postService.DeleteFileByPath(path);
                return Ok();
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        [HttpPut("mark-point")]
        public IActionResult MarkPoint(int id, float point)
        {
            try
            {
                _postService.MarkPoint(id, point);
                return Ok();
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        [HttpPut("submit-homework")]
        public IActionResult SubmitHomework(int userId, int postId, string filepath, string filename, string mimetype)
        {
            try
            {
                _postService.SubmitHomework(userId, postId, filepath, filename, mimetype);
                return Ok();
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        [HttpGet("get-all-homework-by-postId")]
        public IActionResult GetAllHomeWorkByPostId(int postId)
        {
            try
            {
                var result = _postService.GetAllHomeWorkByPostId(postId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

    }
}
