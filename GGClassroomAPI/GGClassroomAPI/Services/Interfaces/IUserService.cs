﻿using GGClassroomAPI.Dtos.Users;
using GGClassroomAPI.Entities;

namespace GGClassroomAPI.Services.Interfaces
{
    public interface IUserService
    {
        void Create(CreateUserDto input);
        LoginResultDto Login(LoginDto input);
        void UpdateInfo(UpdateUserDto input);
        User GetbyId(int id);
    }
}
