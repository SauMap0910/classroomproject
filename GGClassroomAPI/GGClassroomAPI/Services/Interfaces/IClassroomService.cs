﻿using GGClassroomAPI.Dtos.Classrooms;
using GGClassroomAPI.Entities;

namespace GGClassroomAPI.Services.Interfaces
{
    public interface IClassroomService
    {
        void CreateClass(CreateClassroomDto input, int userId);
        void UpdateClass(UpdateClassroomDto input);
        void DeleteClass(int id);
        List<Classroom> GetAllClassroom(int userId);
        void AddUserToClassByEmail(string email, int classId);
        void JoinClassByClassCode(int userId, string classCode);
        List<User> GetAllStudentInClass(int classId);
        User GetHostOfClass(int classId);
    }
}
