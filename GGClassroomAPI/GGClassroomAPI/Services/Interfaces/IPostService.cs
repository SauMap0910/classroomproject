﻿using GGClassroomAPI.Dtos.Post;
using GGClassroomAPI.Entities;

namespace GGClassroomAPI.Services.Interfaces
{
    public interface IPostService
    {
        void CreatePost(CreatePostDto input, int userId);
        void UpdatePost(UpdatePostDto input);
        void DeletePost(int id);
        List<PostResultDto> GetAllPost(int classId);
        string[] GetFiles(string url);
        FileResultDto GetFileByPath(string path);
        void DeleteFileByPath(string path);
        void MarkPoint(int id, float point);
        void SubmitHomework(int userId, int postId, string filePath, string fileName, string mimeType);
        List<HomeworkResultDto> GetAllHomeWorkByPostId(int postId);
    }
}
