﻿using GGClassroomAPI.Constants;
using GGClassroomAPI.DbContexts;
using GGClassroomAPI.Dtos.Users;
using GGClassroomAPI.Entities;
using GGClassroomAPI.Exceptions;
using GGClassroomAPI.Services.Interfaces;
using GGClassroomAPI.Utils;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace GGClassroomAPI.Services.Implements
{
    public class UserService : IUserService
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _dbContext;
        private readonly IConfiguration _configuration;

        public UserService(
            ILogger<UserService> logger,
            ApplicationDbContext dbContext,
            IConfiguration configuration)
        {
            _logger = logger;
            _dbContext = dbContext;
            _configuration = configuration;
        }

        // Auth
        /// <summary>
        /// Đăng ký tài khoản
        /// </summary>
        /// <param name="input"></param>
        /// <exception cref="UserFriendlyException"></exception>
        public void Create(CreateUserDto input)
        {
            if (_dbContext.Users.Any(u => u.Username == input.Username))
            {
                throw new UserFriendlyException($"Tên tài khoản \"{input.Username}\" đã tồn tại");
            }
            _dbContext.Users.Add(new User
            {
                Username = input.Username,
                Password = CommonUtils.CreateMD5(input.Password),
                UserType = input.UserType
            });
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Đăng nhập tài khoản, trả về token và userData
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        /// <exception cref="UserFriendlyException"></exception>
        public LoginResultDto Login(LoginDto input)
        {
            var user = _dbContext.Users.FirstOrDefault(u => u.Username == input.Username);
            if (user == null)
            {
                throw new UserFriendlyException($"Tài khoản \"{input.Username}\" không tồn tại");
            }

            if (CommonUtils.CreateMD5(input.Password) == user.Password)
            {
                var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));

                var claims = new List<Claim>
                {
                    new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                    new Claim(JwtRegisteredClaimNames.Name, user.Username),
                    new Claim(CustomClaimTypes.UserType, user.UserType.ToString())
                };

                var token = new JwtSecurityToken(
                    issuer: _configuration["JWT:ValidIssuer"],
                    audience: _configuration["JWT:ValidAudience"],
                    expires: DateTime.Now.AddSeconds(_configuration.GetValue<int>("JWT:Expires")),
                    claims: claims,
                    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                );

                var bearerToken = $"Bearer {new JwtSecurityTokenHandler().WriteToken(token)}";

                var userData = new UserDataDto()
                {
                    Username = user.Username,
                    UserType = user.UserType,
                    Email = user.Email,
                    Displayname = user.Displayname,
                    Avatar = user.Avatar,
                    Id = user.Id,
                };

                return new LoginResultDto()
                {
                    Token = bearerToken,
                    UserData = userData
                };
            }
            else
            {
                throw new UserFriendlyException($"Mật khẩu không chính xác");
            }
        }

        /// <summary>
        /// Cập nhật thông tin cá nhân
        /// </summary>
        /// <param name="input"></param>
        /// <exception cref="UserFriendlyException"></exception>
        public void UpdateInfo(UpdateUserDto input)
        {
            var userUpdate = _dbContext.Users.FirstOrDefault(u => u.Id == input.Id);
            if (userUpdate == null)
            {
                throw new UserFriendlyException($"Tài khoản không tồn tại");
            }
            //var isAlreadyEmail = _dbContext.Users.FirstOrDefault(u => u.Id == input.Id && u.Email != input.Email);
            //if (isAlreadyEmail != null)
            //{
            //    throw new UserFriendlyException($"Email này đã được đăng ký");
            //}

            userUpdate.Email = input.Email;
            userUpdate.Displayname = input.Displayname;
            userUpdate.Avatar = input.Avatar;
            _dbContext.SaveChanges();
        }

        public User GetbyId(int id)
        {
            var user = _dbContext.Users.FirstOrDefault((p) => p.Id == id);
            return user;
        }
    }
}
