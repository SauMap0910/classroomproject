﻿using GGClassroomAPI.Constants;
using GGClassroomAPI.DbContexts;
using GGClassroomAPI.Dtos.Post;
using GGClassroomAPI.Entities;
using GGClassroomAPI.Exceptions;
using GGClassroomAPI.Migrations;
using GGClassroomAPI.Services.Interfaces;
using GGClassroomAPI.Utils;
using Microsoft.AspNetCore.StaticFiles;
using System.Text.Json;
using System.Web;

namespace GGClassroomAPI.Services.Implements
{
    public class PostService : IPostService
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _dbContext;
        private readonly IConfiguration _configuration;
        private readonly IClassroomService _classroomService;

        public PostService(
            ILogger<PostService> logger,
            ApplicationDbContext dbContext,
            IConfiguration configuration,
            IClassroomService classroomService)
        {
            _logger = logger;
            _dbContext = dbContext;
            _configuration = configuration;
            _classroomService = classroomService;
        }

        /// <summary>
        /// Thêm mới Post
        /// </summary>
        /// <param name="input"></param>
        public void CreatePost(CreatePostDto input, int userId)
        {
            var studentIdList = new List<int>();
            var postInsert = new Post
            {
                Title = input.Title,
                Body = input.Body,
                Topic = input.Topic,
                ClassId = input.ClassId,
                AuthorId = userId,
                CreatedDate = DateTime.Now,
            };
            if (input.IsHomeWork == true) // update trường MaxPoint và ExpireDate nếu là HomeWork
            {
                postInsert.MaxPoint = input.MaxPoint;
                postInsert.ExpireDate = input.ExpireDate;
                postInsert.IsHomeWork = PostStatus.HOMEWORK;
                // Lấy danh sách sinh viên
                var studentList = _classroomService.GetAllStudentInClass(input.ClassId);
                if (studentList != null)
                {
                    studentIdList = studentList.Select(s => s.Id).ToList();
                }
            }
            _dbContext.Posts.Add(postInsert);
            _dbContext.SaveChanges();

            // Lấy tên lớp để tạo đường dẫn thư mục và tìm host
            var classFind = _dbContext.Classrooms.FirstOrDefault(c => c.Id == input.ClassId);
            if (classFind == null)
            {
                throw new UserFriendlyException($"Không tìm thấy lớp học Id = {input.ClassId}");
            }
            // Giao bài tập cho sinh viên
            foreach (var item in studentIdList)
            {
                var isAssigned = _dbContext.UserHomeworks.FirstOrDefault(u => u.UserId == item && u.PostId == postInsert.Id);
                if (isAssigned != null)
                {
                    throw new UserFriendlyException($"Sinh viên id={item} đã được giao bài");
                }
                var UserClassroomFind = _dbContext.UserClassrooms.FirstOrDefault(uc => uc.UserId == item && uc.ClassroomId == classFind.Id);
                if (UserClassroomFind != null)
                {
                    if (UserClassroomFind.IsHost == false || true) // nếu không phải là host thì mới giao bài
                    {
                        var userHomeworkInsert = new UserHomework
                        {
                            UserId = item,
                            PostId = postInsert.Id,
                            ClassId = postInsert.ClassId ?? 0,
                            MaxPoint = input.MaxPoint ?? 100
                        };
                        _dbContext.UserHomeworks.Add(userHomeworkInsert);
                    }
                }
            }
            
            // Xử lý file
            if (input.Attachments != null)
            {
                string uploads = Path.Combine(_configuration["Attachments"], classFind.Id.ToString(), postInsert.Id.ToString()); // url = Attachments\[classId]\[postId]
                string[] fileArray = SaveFiles(input.Attachments, uploads);
                postInsert.Attachment = uploads;
            }
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Cập nhật Post
        /// </summary>
        /// <param name="input"></param>
        /// <exception cref="UserFriendlyException"></exception>
        public void UpdatePost(UpdatePostDto input)
        {
            var postFind = _dbContext.Posts.FirstOrDefault(c => c.Id == input.Id);
            if (postFind == null)
            {
                throw new UserFriendlyException($"Không tìm thấy bài đăng");
            }
            // update thông tin chung
            postFind.Title = input.Title;
            postFind.Body = input.Body;
            postFind.Topic = input.Topic;
            if (input.IsHomeWork == true)
            {
                postFind.MaxPoint = input.MaxPoint;
                postFind.ExpireDate = input.ExpireDate;
            }
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Xóa Post
        /// </summary>
        /// <param name="id"></param>
        /// <exception cref="UserFriendlyException"></exception>
        public void DeletePost(int id)
        {
            var postFind = _dbContext.Posts.FirstOrDefault(c => c.Id == id);
            if (postFind == null)
            {
                throw new UserFriendlyException($"Không tìm thấy bài đăng");
            }
            _dbContext.Posts.Remove(postFind);
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Xem danh sách Post
        /// </summary>
        /// <param name="classId"></param>
        /// <returns></returns>
        public List<PostResultDto> GetAllPost(int classId)
        {
            var postQuery = (from p in _dbContext.Posts
                             join u in _dbContext.Users on p.AuthorId equals u.Id
                             //from uResult in us.DefaultIfEmpty()
                             where p.ClassId == classId
                             select new PostResultDto
                             {
                                 Id = p.Id,
                                 Title = p.Title,
                                 Body = p.Body,
                                 Topic = p.Topic,
                                 IsHomeWork = p.IsHomeWork,
                                 MaxPoint = p.MaxPoint,
                                 ExpireDate = p.ExpireDate,
                                 Attachment = p.Attachment,
                                 ClassId = p.ClassId,
                                 AuthorId = p.AuthorId,
                                 DisplayName = u.Displayname,
                                 Avatar = u.Avatar,
                                 CreatedDate = p.CreatedDate,
                             }).OrderByDescending(p => p.Id).ToList();
            return postQuery;
        }

        /// <summary>
        /// Hàm xử lý lưu files
        /// </summary>
        /// <param name="files"></param>
        /// <param name="uploads"></param>
        /// <returns></returns>
        public string[] SaveFiles(List<IFormFile> files, string path)
        {
            Directory.CreateDirectory(path);
            foreach (IFormFile file in files)
            {
                if (file.Length > 0)
                {
                    string filePath = Path.Combine(path, file.FileName);
                    using (Stream fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write))
                    {
                        file.CopyTo(fileStream);
                    }
                }
            }
            _logger.LogInformation($"fileName = {JsonSerializer.Serialize(Directory.GetFiles(path))}");
            return Directory.GetFiles(path);
        }

        /// <summary>
        /// Lấy ra các file trong đường dẫn
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public string[] GetFiles(string url)
        {
            return Directory.GetFiles(url);
        }

        /// <summary>
        /// Lấy ra file qua đường dẫn
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        /// <exception cref="UserFriendlyException"></exception>
        public FileResultDto GetFileByPath(string path)
        {
            if (!File.Exists(path))
            {
                throw new UserFriendlyException($"Không tìm thấy file"); 
            }
            var result = new FileResultDto();
            result.FileContents = File.ReadAllBytes(path);
            result.FileName = Path.GetFileName(path);
            string contentType;
            new FileExtensionContentTypeProvider().TryGetContentType(result.FileName, out contentType);
            result.ContentType = contentType ?? "application/octet-stream";
            return result;
        }

        /// <summary>
        /// Xóa file
        /// </summary>
        /// <param name="path"></param>
        /// <exception cref="UserFriendlyException"></exception>
        public void DeleteFileByPath(string path)
        {
            if (!File.Exists(path))
            {
                throw new UserFriendlyException($"Không tìm thấy file");
            }
            File.Delete(path);
        }

        /// <summary>
        /// Chấm điểm
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="postId"></param>
        /// <param name="point"></param>
        /// <exception cref="UserFriendlyException"></exception>
        public void MarkPoint(int id, float point)
        {
            var userHomeworkFind = _dbContext.UserHomeworks.FirstOrDefault(u => u.Id == id);
            if (userHomeworkFind == null)
            {
                throw new UserFriendlyException($"Không tìm thấy bài chấm");
            }
            userHomeworkFind.Point = point;
            _dbContext.SaveChanges();
        }

        public void SubmitHomework(int userId, int postId, string filePath, string fileName, string mimeType)
        {
            var userHomeworkFind = _dbContext.UserHomeworks.FirstOrDefault(u => u.UserId == userId && u.PostId == postId);
            if (userHomeworkFind == null)
            {
                throw new UserFriendlyException($"Undefined");
            }
            userHomeworkFind.FilePath = filePath;
            userHomeworkFind.FileName = fileName;
            userHomeworkFind.MimeType = mimeType;
            _dbContext.SaveChanges();
        }

        public List<HomeworkResultDto> GetAllHomeWorkByPostId(int postId)
        {
            return (from h in _dbContext.UserHomeworks
                    join u in _dbContext.Users on h.UserId equals u.Id
                            where h.PostId == postId
                            select new HomeworkResultDto
                            {
                                Id = h.Id,
                                UserId = h.UserId,
                                PostId = postId,
                                Point = h.Point,
                                MaxPoint = h.MaxPoint,
                                ClassId = h.ClassId,
                                FilePath = h.FilePath,
                                FileName = h.FileName,
                                MimeType = h.MimeType,
                                Displayname = u.Displayname,
                                Avatar = u.Avatar,
                            }).OrderByDescending(p => p.Id).ToList();
        }
    }
}
