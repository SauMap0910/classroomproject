﻿using GGClassroomAPI.DbContexts;
using GGClassroomAPI.Dtos.Classrooms;
using GGClassroomAPI.Entities;
using GGClassroomAPI.Exceptions;
using GGClassroomAPI.Services.Interfaces;
using GGClassroomAPI.Utils;
using System.Security.Claims;
using Microsoft.Net.Http.Headers;
using Microsoft.EntityFrameworkCore;

namespace GGClassroomAPI.Services.Implements
{
    public class ClassroomService : IClassroomService
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _dbContext;
        private readonly IConfiguration _configuration;

        public ClassroomService(
            ILogger<ClassroomService> logger,
            ApplicationDbContext dbContext,
            IConfiguration configuration)
        {
            _logger = logger;
            _dbContext = dbContext;
            _configuration = configuration;
        }

        /// <summary>
        /// Thêm mới lớp học
        /// </summary>
        /// <param name="input"></param>
        /// <param name="userId"></param>
        /// <exception cref="UserFriendlyException"></exception>
        public void CreateClass(CreateClassroomDto input, int userId)
        {
            if (userId == 0)
            {
                throw new UserFriendlyException($"Tài khoản đăng nhập không tồn tại");
            }
            var classInsert = new Classroom
            {
                ClassName = input.ClassName,
                Content = input.Content,
                Description = input.Description,
                Room = input.Room,
                ClassCode = CommonUtils.RandomString(7)
            };
            _dbContext.Classrooms.Add(classInsert);
            _dbContext.SaveChanges(); // Lưu để lấy id cho host
            // Tạo liên kết n-n UserClassroom và gán host
            _dbContext.UserClassrooms.Add(new UserClassroom
            {
                UserId = userId,
                ClassroomId = classInsert.Id,
                IsHost = true
            });
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Cập nhật lớp học
        /// </summary>
        /// <param name="input"></param>
        /// <exception cref="UserFriendlyException"></exception>
        public void UpdateClass(UpdateClassroomDto input)
        {
            var classFind = _dbContext.Classrooms.FirstOrDefault(c => c.Id == input.Id);
            if (classFind == null)
            {
                throw new UserFriendlyException($"Không tìm thấy lớp học");
            }
            classFind.ClassName = input.ClassName;
            classFind.Content = input.Content;
            classFind.Description = input.Description;
            classFind.Room = input.Room;
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Xóa lớp học
        /// </summary>
        /// <param name="id"></param>
        /// <exception cref="UserFriendlyException"></exception>
        public void DeleteClass(int id)
        {
            var classFind = _dbContext.Classrooms.FirstOrDefault(c => c.Id == id);
            if (classFind == null)
            {
                throw new UserFriendlyException($"Không tìm thấy lớp học");
            }
            // Xoá lớp học, xóa cả ở bảng nối
            var userClassroomsQuery = _dbContext.UserClassrooms.AsQueryable()
                                .Where(c => c.ClassroomId == id);
            _dbContext.UserClassrooms.RemoveRange(userClassroomsQuery);
            _dbContext.Classrooms.Remove(classFind);
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Lấy danh sách lớp học qua Token id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<Classroom> GetAllClassroom(int userId)
        {
            var classQuery = (from c in _dbContext.Classrooms
                              join uc in _dbContext.UserClassrooms on c.Id equals uc.ClassroomId
                              where uc.UserId == userId
                              select new Classroom
                              {
                                  Id = c.Id,
                                  ClassName = c.ClassName,
                                  ClassCode = c.ClassCode,
                                  Content = c.Content,
                                  Description = c.Description,
                                  Room = c.Room,
                              }).OrderByDescending(p => p.Id).ToList();
            return classQuery;
        }

        /// <summary>
        /// Thêm người dùng vào lớp học qua email
        /// </summary>
        /// <param name="email"></param>
        /// <param name="classId"></param>
        /// <exception cref="UserFriendlyException"></exception>
        public void AddUserToClassByEmail(string email, int classId)
        {
            var userFind = _dbContext.Users.FirstOrDefault(c => c.Email == email);
            if (userFind == null)
            {
                throw new UserFriendlyException($"Email người dùng không tồn tại");
            }
            var classFind = _dbContext.Classrooms.FirstOrDefault(c => c.Id == classId);
            if (classFind == null)
            {
                throw new UserFriendlyException($"Không tìm thấy lớp học");
            }
            // Nếu sv đã có trong lớp học rồi thì không thêm
            var isJoined = _dbContext.UserClassrooms.FirstOrDefault(uc => uc.UserId == userFind.Id && uc.ClassroomId == classId);
            if (isJoined != null)
            {
                throw new UserFriendlyException($"Sinh viên đã tồn tại trong lớp học");
            }
            _dbContext.UserClassrooms.Add(new UserClassroom
            {
                UserId = userFind.Id,
                ClassroomId = classFind.Id,
                IsHost = false
            });
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Tham gia lớp học qua classCode
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="classCode"></param>
        /// <exception cref="UserFriendlyException"></exception>
        public void JoinClassByClassCode(int userId, string classCode)
        {
            var classFind = _dbContext.Classrooms.FirstOrDefault(c => c.ClassCode == classCode);
            if (classFind == null)
            {
                throw new UserFriendlyException($"Không tìm thấy lớp học");
            }
            // Nếu sv đã có trong lớp học rồi thì không thêm
            var isJoined = _dbContext.UserClassrooms.FirstOrDefault(uc => uc.UserId == userId && uc.ClassroomId == classFind.Id);
            if (isJoined != null)
            {
                throw new UserFriendlyException($"Sinh viên đã tồn tại trong lớp học");
            }
            _dbContext.UserClassrooms.Add(new UserClassroom
            {
                UserId = userId,
                ClassroomId = classFind.Id,
                IsHost = false
            });
            _dbContext.SaveChanges();
        }

        public List<User> GetAllStudentInClass(int classId)
        {
            var studentQuery = (from u in _dbContext.Users
                              join uc in _dbContext.UserClassrooms on u.Id equals uc.UserId
                              where uc.ClassroomId == classId
                              select new User
                              {
                                  Id = u.Id,
                                  Username = u.Username,
                                  Email = u.Email,
                                  Displayname = u.Displayname,
                                  Avatar = u.Avatar,
                              }).ToList();
            return studentQuery;
        }

        public User GetHostOfClass(int classId)
        {
            var host = _dbContext.UserClassrooms.FirstOrDefault(h => h.ClassroomId == classId && h.IsHost == true);
            var hostFind = _dbContext.Users.FirstOrDefault(h => h.Id == host.UserId);
            return hostFind;
        }
    }
}
