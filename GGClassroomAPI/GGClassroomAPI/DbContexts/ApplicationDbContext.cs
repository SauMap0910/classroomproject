﻿using GGClassroomAPI.Constants;
using GGClassroomAPI.Entities;
using Microsoft.EntityFrameworkCore;

namespace GGClassroomAPI.DbContexts
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Classroom> Classrooms { get; set; }
        public DbSet<UserClassroom> UserClassrooms { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<UserHomework> UserHomeworks { get; set; }

        public ApplicationDbContext() { }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
    :   base(options)
        { }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("User");
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .IsRequired();
                entity.Property(e => e.Username)
                    .IsUnicode(false)
                    .HasMaxLength(50)
                    .IsRequired();
                entity.Property(e => e.Password)
                    .IsUnicode()
                    .HasMaxLength(100)
                    .IsRequired();
                entity.Property(e => e.UserType)
                    .HasDefaultValue(UserTypes.Customer)
                    .IsRequired();

                // Thông tin chi tiết
                entity.Property(e => e.Email)
                    .IsRequired(false);
                entity.Property(e => e.Displayname)
                    .IsUnicode()
                    .HasMaxLength(50)
                    .IsRequired(false);
                entity.Property(e => e.Avatar)
                    .IsRequired(false);
            });

            modelBuilder.Entity<Classroom>(entity =>
            {
                entity.ToTable("Classroom");
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .IsRequired();
                entity.Property(e => e.ClassName)
                    .IsUnicode(false)
                    .HasMaxLength(50)
                    .IsRequired();
                entity.Property(e => e.Content)
                    .IsUnicode()
                    .HasMaxLength(50)
                    .IsRequired(false);
                entity.Property(e => e.Description)
                    .IsRequired(false);
                entity.Property(e => e.Room)
                    .IsRequired(false);
                entity.Property(e => e.ClassCode)
                    .IsRequired();
            });

            modelBuilder.Entity<UserClassroom>(entity =>
            {
                entity.ToTable("UserClassroom");
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .IsRequired();
                entity.Property(e => e.UserId)
                    .IsRequired();
                entity.Property(e => e.ClassroomId)
                    .IsRequired();
                entity.Property(e => e.IsHost)
                    .HasDefaultValue(false);

                /*entity.HasOne<User>()
                    .WithMany()
                    .HasForeignKey(p => p.UserId);
                entity.HasOne<Classroom>()
                    .WithMany()
                    .HasForeignKey(p => p.ClassroomId);*/
            });

            modelBuilder.Entity<Post>(entity =>
            {
                entity.ToTable("Post");
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .IsRequired();
                entity.Property(e => e.Title)
                    .IsUnicode()
                    .HasMaxLength(50)
                    .IsRequired();
                entity.Property(e => e.Body)
                    .IsUnicode(false)
                    .HasMaxLength(50)
                    .IsRequired(false);
                entity.Property(e => e.Topic)
                    .IsRequired(false);
                entity.Property(e => e.IsHomeWork)
                    .HasDefaultValue(false);
                entity.Property(e => e.MaxPoint)
                    .IsRequired(false);
                entity.Property(e => e.ExpireDate)
                    .IsRequired(false);
                entity.Property(e => e.Attachment)
                    .IsRequired(false);
                entity.Property(e => e.ClassId)
                    .IsRequired(false);
                entity.Property(e => e.AuthorId)
                    .IsRequired(false);
                entity.Property(e => e.CreatedDate)
                    .IsRequired();
            });

            modelBuilder.Entity<UserHomework>(entity =>
            {
                entity.ToTable("UserHomework");
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .IsRequired();
                entity.Property(e => e.UserId)
                    .IsRequired();
                entity.Property(e => e.PostId)
                    .IsRequired();
                entity.Property(e => e.ClassId)
                    .IsRequired();
                entity.Property(e => e.Point)
                    .IsRequired(false);
                entity.Property(e => e.MaxPoint)
                    .IsRequired();
                entity.Property(e => e.FilePath)
                    .IsRequired(false);
                entity.Property(e => e.FileName)
                    .IsRequired(false);
                entity.Property(e => e.MimeType)
                    .IsRequired(false);
            });
        }
    }
}
