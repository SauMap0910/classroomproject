﻿using GGClassroomAPI.Entities;
using Microsoft.Net.Http.Headers;
using System;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Net.Http.Headers;
using System.IdentityModel.Tokens.Jwt;
using System.Net;

namespace GGClassroomAPI.Utils
{
    public static class CommonUtils
    {
        private static Random random = new Random();

        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            MD5 md5 = MD5.Create();
            byte[] inputBytes = Encoding.UTF8.GetBytes(input);
            byte[] hashBytes = md5.ComputeHash(inputBytes);
            return Convert.ToHexString(hashBytes);
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static int GetUserIdByToken(string authorization)
        {
            var token = authorization.Replace("Bearer ", "");
            var handler = new JwtSecurityTokenHandler();
            var jwtSecurityToken = handler.ReadJwtToken(token);
            return Convert.ToInt32(jwtSecurityToken.Claims.First(claim => claim.Type == "sub").Value);
        }
    }
}
