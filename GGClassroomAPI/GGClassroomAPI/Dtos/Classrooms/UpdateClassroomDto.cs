﻿namespace GGClassroomAPI.Dtos.Classrooms
{
    public class UpdateClassroomDto : CreateClassroomDto
    {
        public int Id { get; set; }
    }
}
