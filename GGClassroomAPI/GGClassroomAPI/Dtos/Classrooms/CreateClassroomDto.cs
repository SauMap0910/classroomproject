﻿using System.ComponentModel.DataAnnotations;

namespace GGClassroomAPI.Dtos.Classrooms
{
    public class CreateClassroomDto
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Tên lớp học không được bỏ trống")]
        public string ClassName { get; set; }
        public string Content { get; set; }
        public string Description { get; set; }
        public string Room { get; set; }
    }
}
