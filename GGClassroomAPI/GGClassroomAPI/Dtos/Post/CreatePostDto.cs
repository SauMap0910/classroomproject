﻿namespace GGClassroomAPI.Dtos.Post
{
    public class CreatePostDto
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public string Topic { get; set; }
        public int ClassId { get; set; }

        // HomeWork
        public bool IsHomeWork { get; set; }
        public int? MaxPoint { get; set; }
        public DateTime? ExpireDate { get; set; }
        public List<IFormFile> Attachments { get; set; }
    }
}
