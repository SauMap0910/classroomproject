﻿namespace GGClassroomAPI.Dtos.Post
{
    public class UpdatePostDto : CreatePostDto
    {
        public int Id { get; set; }
    }
}
