﻿using GGClassroomAPI.Entities;

namespace GGClassroomAPI
{
    public class PostResultDto : Post
    {
        public string DisplayName { get; set; }
        public string Avatar { get; set; }
    }
}
