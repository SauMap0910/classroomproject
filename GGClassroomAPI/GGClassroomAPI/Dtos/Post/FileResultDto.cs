﻿namespace GGClassroomAPI.Dtos.Post
{
    public class FileResultDto
    {
        public byte[] FileContents { get; set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }
    }
}
