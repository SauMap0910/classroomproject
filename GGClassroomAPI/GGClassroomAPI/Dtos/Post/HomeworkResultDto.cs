﻿using GGClassroomAPI.Entities;

namespace GGClassroomAPI.Dtos.Post
{
    public class HomeworkResultDto : UserHomework
    {
        public string Displayname { get; set; }
        public string Avatar { get; set; }
    }
}
