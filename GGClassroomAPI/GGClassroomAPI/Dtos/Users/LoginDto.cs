﻿using System.ComponentModel.DataAnnotations;

namespace GGClassroomAPI.Dtos.Users
{
    public class LoginDto
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Tên đăng nhập không được bỏ trống")]
        public string Username { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Mật khẩu không được bỏ trống")]
        public string Password { get; set; }
    }
}
