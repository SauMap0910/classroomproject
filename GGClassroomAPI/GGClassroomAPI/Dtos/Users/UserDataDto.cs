﻿namespace GGClassroomAPI.Dtos.Users
{
    public class UserDataDto
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public int UserType { get; set; }

        // Thông tin chi tiết
        public string Email { get; set; }
        public string Displayname { get; set; }
        public string Avatar { get; set; }
    }
}
