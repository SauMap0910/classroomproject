﻿namespace GGClassroomAPI.Dtos.Users
{
    public class LoginResultDto
    {
        public string Token { get; set; }
        public UserDataDto UserData { get; set; }
    }
}
