﻿namespace GGClassroomAPI.Dtos.Users
{
    public class UpdateUserDto
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Displayname { get; set; }
        public string Avatar { get; set; }
    }
}
