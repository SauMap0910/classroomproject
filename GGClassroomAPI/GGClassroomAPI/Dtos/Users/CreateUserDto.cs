﻿using System.ComponentModel.DataAnnotations;

namespace GGClassroomAPI.Dtos.Users
{
    public class CreateUserDto
    {
        private string _username;
        private string _password;

        [Required(AllowEmptyStrings = false, ErrorMessage = "Tên đăng nhập không được bỏ trống")]
        public string Username
        {
            get => _username;
            set => _username = value?.Trim();
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Mật khẩu không được bỏ trống")]
        public string Password
        {
            get => _password;
            set => _password = value?.Trim();
        }

        public int UserType { get; set; }
    }
}
