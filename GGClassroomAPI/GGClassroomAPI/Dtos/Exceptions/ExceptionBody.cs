﻿namespace GGClassroomAPI.Dtos.Exceptions
{
    public class ExceptionBody
    {
        public string Message { get; set; }
    }
}
