﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GGClassroomAPI.Migrations
{
    public partial class ThembangPost : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Post",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    Body = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Topic = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsHomeWork = table.Column<bool>(type: "bit", nullable: false, defaultValue: false),
                    MaxPoint = table.Column<int>(type: "int", nullable: true),
                    ExpireDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Attachment = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Post", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Post");
        }
    }
}
