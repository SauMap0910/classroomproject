﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GGClassroomAPI.Migrations
{
    public partial class ThemCot_MaxPoint_UserHomework : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<float>(
                name: "MaxPoint",
                table: "UserHomework",
                type: "real",
                nullable: false,
                defaultValue: 0f);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MaxPoint",
                table: "UserHomework");
        }
    }
}
