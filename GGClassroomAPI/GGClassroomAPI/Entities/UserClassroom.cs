﻿namespace GGClassroomAPI.Entities
{
    public class UserClassroom
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ClassroomId { get; set; }
        public bool IsHost { get; set; }
    }
}
