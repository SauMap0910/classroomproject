﻿namespace GGClassroomAPI.Entities
{
    public class Classroom
    {
        public int Id { get; set; }
        public string ClassName { get; set; }
        public string ClassCode { get; set; }
        
        // Chi tiết lớp học
        public string Content { get; set; }
        public string Description { get; set; }
        public string Room { get; set; }
    }
}
