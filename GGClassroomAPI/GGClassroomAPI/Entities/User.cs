﻿namespace GGClassroomAPI.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int UserType { get; set; }

        // Thông tin chi tiết
        public string Email { get; set; }
        public string Displayname { get; set; }
        public string Avatar { get; set; }
    }
}
