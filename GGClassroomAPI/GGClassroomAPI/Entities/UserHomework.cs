﻿namespace GGClassroomAPI.Entities
{
    public class UserHomework
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int PostId { get; set; }
        public int ClassId { get; set; }
        public float? Point { get; set; }
        public float MaxPoint { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public string MimeType { get; set; }
    }
}
