﻿namespace GGClassroomAPI.Entities
{
    public class Post
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string Topic { get; set; }
        public int? ClassId { get; set; }
        public int? AuthorId { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;

        // HomeWork
        public bool IsHomeWork { get; set; }
        public int? MaxPoint { get; set; }
        public DateTime? ExpireDate { get; set; }
        public string Attachment { get; set; }
    }
}
